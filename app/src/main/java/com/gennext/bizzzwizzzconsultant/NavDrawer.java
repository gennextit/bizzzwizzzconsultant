package com.gennext.bizzzwizzzconsultant;

/**
 * Created by Abhijit on 14-Jul-16.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.gennext.bizzzwizzzconsultant.app.Domains;
import com.gennext.bizzzwizzzconsultant.app.ViewTimeSlot;
import com.gennext.bizzzwizzzconsultant.common.AppFeedback;
import com.gennext.bizzzwizzzconsultant.app.forgot.ForgotPass;
import com.gennext.bizzzwizzzconsultant.util.PDialog;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public abstract class NavDrawer extends AppCompatActivity {
    private ProgressDialog navPDialog;
    private DrawerLayout dLayout;
    private AlertDialog aDialog;
    private LinearLayout dMenuLayout;
    protected abstract String setNavDrawer(Toolbar toolbar);
    protected abstract Fragment setDefaultScreen(Fragment fragment);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_drawer);
        setNavDrawer();
        setDefaultScreen();
    }

    private void setDefaultScreen() {
        Fragment fragment=setDefaultScreen(null);
        addFragmentWithoutBackstack(fragment,R.id.container_main,"defaultScreen");
        setDefaultScreen(fragment);
    }

    public void setNavDrawer(){
        Toolbar toolbar=null;
        String title=setNavDrawer(toolbar);
        if(title!=null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setNavDrawer(toolbar);
            toolbar.setTitle(title);
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.mipmap.ic_menu_white);
        }
        SetDrawer(NavDrawer.this,toolbar);
    }

    public boolean APICheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return true;
        } else {
            return false;
        }
    }
    public void closeFragmentDialog(String dialogName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }


    public void hideKeybord(Activity act) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(act.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }


    public void showPDialog(String msg) {
        navPDialog = new ProgressDialog(NavDrawer.this);
        navPDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // navPDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.dialog_animation));
        navPDialog.setMessage(msg);
        navPDialog.setIndeterminate(false);
        navPDialog.setCancelable(false);
        navPDialog.show();
    }

    public void dismissPDialog() {
        if (navPDialog != null)
            navPDialog.dismiss();
    }


    public String LoadPref(String key) {
        return LoadPref(NavDrawer.this, key);
    }

    public String LoadPref(Context context, String key) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            return sharedPreferences.getString(key, "");
        } else {
            return "";
        }
    }

    public void SavePref(String key, String value) {
        SavePref(NavDrawer.this, key, value);
    }

    public void SavePref(Context context, String key, String value) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.apply();
        }
    }

    public String encodeUrl(String res) {
        String UrlEncoding = null;
        try {
            UrlEncoding = URLEncoder.encode(res, "UTF-8");

        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return UrlEncoding;
    }

    public boolean isOnline() {
        boolean conn = false;
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(NavDrawer.this.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        if (haveConnectedWifi == true || haveConnectedMobile == true) {
            conn = true;
        } else {
            conn = false;
        }

        return conn;
    }


    public String getSt(int id) {

        return getResources().getString(id);
    }


    public void showToast(String txt) {
        // Inflate the Layout
        Toast toast = Toast.makeText(NavDrawer.this, txt, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
        toast.show();
    }


    protected void SetDrawer(final Activity act, Toolbar toolbar) {
        // TODO Auto-generated method stub

        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean drawerOpen = dLayout.isDrawerOpen(dMenuLayout);
                        if (!drawerOpen) {
                            dLayout.openDrawer(dMenuLayout);
                        } else {
                            dLayout.closeDrawer(dMenuLayout);
                        }
                    }
                }

        );

        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dMenuLayout = (LinearLayout) findViewById(R.id.left_menu_drawer);
        LinearLayout llWhitespace = (LinearLayout) findViewById(R.id.ll_whitespace);
        Button btnSlot = (Button) findViewById(R.id.btn_menu_slot);
        Button btnFeedback = (Button) findViewById(R.id.btn_menu_feedback);
        Button btnDomains = (Button) findViewById(R.id.btn_menu_domains);
        Button btnTheme = (Button) findViewById(R.id.btn_menu_theme);
        Button btnLogout = (Button) findViewById(R.id.btn_menu_logout);
        Button btnForgotPass = (Button) findViewById(R.id.btn_menu_pass);
        Switch swReminder = (Switch) findViewById(R.id.sw_menu_reminder);

        llWhitespace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dLayout.closeDrawer(dMenuLayout);
            }
        });
        btnSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dLayout.closeDrawer(dMenuLayout);
                addFragment(ViewTimeSlot.newInstance(act),"viewTimeSlot");
            }
        });
        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dLayout.closeDrawer(dMenuLayout);
                addFragment(AppFeedback.newInstance(act),"appFeedback");
            }
        });
        btnDomains.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dLayout.closeDrawer(dMenuLayout);
                addFragment(Domains.newInstance(act),"domains");
            }
        });
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dLayout.closeDrawer(dMenuLayout);
                Intent intent=new Intent(act,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dLayout.closeDrawer(dMenuLayout);
                Intent intent=new Intent(act,ForgotPassActivity.class);
                intent.putExtra(ForgotPassActivity.TITLE_NAME,"Change Password");
                startActivity(intent);
            }
        });
        btnTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dLayout.closeDrawer(dMenuLayout);

            }
        });
        swReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
    }


    public void showProgressDialog(Activity context, String msg) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressDialog.setMessage(msg);
//        progressDialog.setIndeterminate(false);
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        PDialog cDialog = PDialog.newInstance(context, msg);
        aDialog = cDialog.show();
    }

    public void hideProgressDialog() {
//        if (progressDialog != null)
//            progressDialog.dismiss();
        if (aDialog != null)
            aDialog.dismiss();
    }

    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack("tag");
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragment(Fragment fragment, String tag) {
        addFragment(fragment, android.R.id.content, tag);
    }

    protected void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragmentWithoutBackstack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.commit();
    }

    // for 2 tabs
    public void setTabFragment(int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1, Fragment rFrag2, Fragment rFrag3) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide 1st fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }
        // Hide 2nd fragments
        if (rFrag2 != null && rFrag2.isAdded()) {
            ft.hide(rFrag2);
        }
        // Hide 2nd fragments
        if (rFrag3 != null && rFrag3.isAdded()) {
            ft.hide(rFrag3);
        }

        ft.commit();
    }
}