package com.gennext.bizzzwizzzconsultant;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import java.util.Calendar;

import static com.gennext.bizzzwizzzconsultant.MainActivity.NOTI_REQ;

/**
 * Created by Abhijit-PC on 27-Feb-17.
 */
public class ReminderActivity extends BaseActivity {

    @Override
    protected String setTitle(Toolbar toolbar) {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        intUi();
    }

    private void intUi() {
        final EditText etHour=(EditText)findViewById(R.id.et_hour);
        final EditText etMin=(EditText)findViewById(R.id.et_min);

        findViewById(R.id.btnSetReminder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setReminder(Integer.parseInt(etHour.getText().toString()),Integer.parseInt(etMin.getText().toString()));
            }
        });
    }

    private void setReminder(int hour, int min) {
        Calendar cal = java.util.Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.clear();
        cal.set(Calendar.YEAR, 2017);
        cal.set(Calendar.DAY_OF_MONTH, 26);
        cal.set(Calendar.MONTH, 1);

        cal.set(Calendar.HOUR_OF_DAY,hour);
        cal.set(Calendar.MINUTE,min);
        cal.set(Calendar.SECOND, 00);

        Intent intent = new Intent(getApplicationContext(), NotificationReciver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), NOTI_REQ, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager= (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,cal.getTimeInMillis(),AlarmManager.INTERVAL_DAY,pendingIntent);

    }
}
