package com.gennext.bizzzwizzzconsultant.model;


/**
 * Created by Abhijit on 08-Aug-16.
 */


import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.app.ViewTimeSlot;
import com.gennext.bizzzwizzzconsultant.util.DateTimeUtility;

import java.util.ArrayList;


public class TimeSlotAdapter extends ArrayAdapter<TimeSlotModel> {
    private final ArrayList<TimeSlotModel> list;
    private final ViewTimeSlot parentClass;

    private Activity context;

    public TimeSlotAdapter(Activity context, int textViewResourceId, ArrayList<TimeSlotModel> list, ViewTimeSlot domains) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.parentClass = domains;
        this.list = list;
    }

    class ViewHolder {
        private LinearLayout slotLayout;
        private ImageButton buttonMore;
        private TextView tvDate,tvSTime,tvETime;

        public ViewHolder(View v) {
            slotLayout = (LinearLayout) v.findViewById(R.id.slot);
            tvDate = (TextView) v.findViewById(R.id.tv_date);
            tvSTime = (TextView) v.findViewById(R.id.tv_sTime);
            tvETime = (TextView) v.findViewById(R.id.tv_eTime);
            buttonMore = (ImageButton) v.findViewById(R.id.btnMore);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.slot_timeslot, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        final TimeSlotModel model = list.get(position);

        if (model.getDate() != null) {
            holder.tvDate.setText(DateTimeUtility.convertDateYMD(model.getDate())+"\n("+model.getDay()+")");
        }
        if (model.getStartTime() != null) {
            holder.tvSTime.setText(DateTimeUtility.convertTime24to12Hours(model.getStartTime()));
        }
        if (model.getEndTime() != null) {
            holder.tvETime.setText(DateTimeUtility.convertTime24to12Hours(model.getEndTime()));
        }
        final ViewHolder finalHolder = holder;
        holder.slotLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(finalHolder, model);
            }
        });
        holder.buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(finalHolder, model);
            }
        });


        return v;
    }

    private void showPopup(ViewHolder holder, final TimeSlotModel model) {
        final PopupMenu popup = new PopupMenu(context, holder.buttonMore);
        popup.getMenuInflater().inflate(R.menu.menu_edit_delete, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int i = item.getItemId();
                if (i == R.id.menu_edit_option) {
                    parentClass.editTimeSlot(model);
                    return true;
                } else if (i == R.id.menu_delete_option) {
                    parentClass.deleteTimeSlot(model);
                    return true;
                } else {
                    return onMenuItemClick(item);
                }
            }
        });

        popup.show();
    }

}
