//package com.gennext.bizzzwizzzconsultant.model;
//
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.net.Uri;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.PopupMenu;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.gennext.bizzzwizzzconsultant.R;
//import com.gennext.bizzzwizzzconsultant.app.Dashboard;
//import com.gennext.bizzzwizzzconsultant.util.AppUser;
//import com.gennext.bizzzwizzzconsultant.util.DateTimeUtility;
//
//import java.util.ArrayList;
//
//public class BookingSlotAdapter extends ArrayAdapter<BookingSlot> {
//    private final ArrayList<BookingSlot> list;
//    private final Dashboard parentClass;
//
//    private Activity context;
//
//    public BookingSlotAdapter(Activity context, int textViewResourceId, ArrayList<BookingSlot> list, Dashboard dashboard) {
//        super(context, textViewResourceId, list);
//        this.context = context;
//        this.list = list;
//        this.parentClass=dashboard;
//    }
//
//    private class ViewHolder {
//        View lineBreak;
//        LinearLayout slot;
//        TextView tvName, tvTime, tvDate;
//        ImageView ivCall;
//        ImageButton btnMore;
//    }
//
//    @Override
//    public BookingSlot getItem(int position) {
//        return list.get(position);
//    }
//    @Override
//    public int getItemViewType(int position) {
//        return list.get(position).getItemType();
//    }
//    @Override
//    public int getViewTypeCount() {
//        return 2;
//    }
//
//    @Override
//    public int getCount() {
//        return list.size();
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//
//    @Override
//    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
//        View v = convertView;
//        int rowType = getItemViewType(position);
//        ViewHolder holder = null;
//        if (v == null) {
//            holder = new ViewHolder();
//            // Inflate the layout according to the view type
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////            v = inflater.inflate(R.layout.slot_bookings, parent, false);
//            switch (rowType) {
//                case BookingSlot.TYPE_ITEM:
//                    v = inflater.inflate(R.layout.slot_bookings, null);
//                    holder.slot = (LinearLayout) v.findViewById(R.id.slot);
//                    holder.ivCall = (ImageView) v.findViewById(R.id.iv_call);
//                    holder.tvName = (TextView) v.findViewById(R.id.tv_name);
//                    holder.tvTime = (TextView) v.findViewById(R.id.tv_time);
//                    holder.btnMore = (ImageButton) v.findViewById(R.id.btnMore);
//                    break;
//                case BookingSlot.TYPE_SEPARATOR:
//                    v = inflater.inflate(R.layout.slot_item_seprator, null);
////                    holder.tvDate = (TextView) v.findViewById(R.id.tv_date_saprator);
////                    holder.lineBreak = (View) v.findViewById(R.id.linebreak);
//                    break;
//            }
//            v.setTag(holder);
//        } else {
//            holder = (ViewHolder) v.getTag();
//        }
//        switch (rowType) {
//            case BookingSlot.TYPE_ITEM:
//                switch (getItem(position).getStatus().toLowerCase()){
//                    case "progress":
//                        holder.slot.setBackgroundResource(R.color.status_inprogress);
//                        break;
//                   case "inprocess":
//                        holder.slot.setBackgroundResource(R.color.status_process);
//                        break;
//                    default:
//                        holder.slot.setBackgroundResource(R.color.status_pending);
//                        break;
//
//                }
//                holder.ivCall.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if(getItem(position).getStatus().toLowerCase().equals("progress")) {
//                            String phoneNo = getItem(position).getMobile();
//                            AppUser.setActiveMobileNo(context,phoneNo);
//                            parentClass.callAction(phoneNo);
//
//                        }else if(getItem(position).getStatus().toLowerCase().equals("inprocess")) {
//                            String time=DateTimeUtility.convertTime24to12Hours(getItem(position).getTime());
//                            String date=DateTimeUtility.convertDateYMD2(getItem(position).getDate());
//                            Toast.makeText(context, "Call functionality active on date: "+date, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(context, "Slot is cancelled", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//                if(getItem(position).getName()!=null){
//                    holder.tvName.setText(getItem(position).getName());
//                }
//                if(getItem(position).getTime()!=null){
//                    holder.tvTime.setText(DateTimeUtility.convertTime24to12Hours(getItem(position).getTime()));
//                }
//                final ViewHolder finalHolder = holder;
//                holder.btnMore.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        String mString=getItem(position).getStatus().toLowerCase();
//                        switch (mString){
//                            case "progress":
//                                showPopup(finalHolder,getItem(position),mString);
//                                break;
//                            case "inprocess":
//                                showPopup(finalHolder,getItem(position),mString);
//                                break;
//
//                        }
//                    }
//                });
//                break;
//            case BookingSlot.TYPE_SEPARATOR:
//                if(getItem(position).getDate()!=null){
//                    if(position==0){
//                        holder.lineBreak.setVisibility(View.GONE);
//                    }else{
//                        holder.lineBreak.setVisibility(View.VISIBLE);
//                    }
//                    holder.tvDate.setText(DateTimeUtility.convertDateYMD2(getItem(position).getDate()));
//                }
//                break;
//        }
//        return v;
//    }
//
//    private void showPopup(ViewHolder holder, final BookingSlot sample,String mString) {
//        final PopupMenu popup = new PopupMenu(context, holder.btnMore);
//        switch (mString){
//            case "progress":
//                popup.getMenuInflater().inflate(R.menu.menu_progress, popup.getMenu());
//                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    public boolean onMenuItemClick(MenuItem item) {
//                        int i = item.getItemId();
//                        if (i == R.id.menu_opt_status) {
//                            parentClass.updateSlotStatus(sample);
//                            return true;
//                        } else if (i == R.id.menu_opt_feedback) {
//                            parentClass.sendFeedback(sample);
//                            return true;
//                        } else if(i==R.id.menu_opt_detail){
//                            parentClass.bookingSlotDetail(sample);
//                            return true;
//                        } else if (i == R.id.menu_opt_cancel) {
//                            parentClass.cancelSlot(sample);
//                            return true;
//                        } else {
//                            return onMenuItemClick(item);
//                        }
//                    }
//                });
//                popup.show();
//                break;
//            case "inprocess":
//                popup.getMenuInflater().inflate(R.menu.menu_inprocess, popup.getMenu());
//                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    public boolean onMenuItemClick(MenuItem item) {
//                        int i = item.getItemId();
//                        if(i==R.id.menu_opt_cancel){
//                            parentClass.cancelSlot(sample);
//                            return true;
//                        } else if(i==R.id.menu_opt_detail){
//                            parentClass.bookingSlotDetail(sample);
//                            return true;
//                        } else {
//                            return onMenuItemClick(item);
//                        }
//                    }
//                });
//                popup.show();
//                break;
//
//        }
//
//    }
//
//
//}
