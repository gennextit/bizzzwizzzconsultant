package com.gennext.bizzzwizzzconsultant.model;


import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.app.Dashboard;
import com.gennext.bizzzwizzzconsultant.util.AppUser;
import com.gennext.bizzzwizzzconsultant.util.DateTimeUtility;
import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

public class DashboardAdapter extends ExpandableRecyclerAdapter<DashboardAdapter.BodyListItem> {
    public static final int TYPE_BODY = 1001;
    private ArrayList<BookingSlot> sList;
    private final Dashboard parentClass;
    private ArrayList<BookingSlot> listItems;

    public DashboardAdapter(Context context, Dashboard dashboard, ArrayList<BookingSlot> sList) {
        super(context);
        this.sList=sList;
        this.parentClass=dashboard;
        setItems(getSampleItems(sList));
    }

    public void setListItems(ArrayList<BookingSlot> listItems) {
        sList = listItems;
        setItems(getSampleItems(sList));
        notifyDataSetChanged();
    }


    public static class BodyListItem extends ExpandableRecyclerAdapter.ListItem {
        public String UserId,SlotId,Name,DateSlt,TimeSlt,Mobile,Description,Status;
        int ItemType;

        public BodyListItem(String date) {
            super(TYPE_HEADER);
            DateSlt = date;
        }

        public BodyListItem(String userid, String slotId, String name, String dateSlt, String timeSlt, String mobile,
                            String description, String status,int itemType) {
            super(TYPE_BODY);
            UserId = userid;
            SlotId = slotId;
            Name = name;
            DateSlt = dateSlt;
            TimeSlt = timeSlt;
            Mobile = mobile;
            Description = description;
            Status = status;
            ItemType = itemType;
        }
    }

    public class HeaderViewHolder extends ExpandableRecyclerAdapter.HeaderViewHolder {
        TextView tvDate;

        public HeaderViewHolder(View view) {
            super(view, (ImageView) view.findViewById(R.id.item_arrow));
            tvDate = (TextView) view.findViewById(R.id.item_header_name);
        }

        public void bind(int position) {
            super.bind(position);
            tvDate.setText(DateTimeUtility.convertDateYMD2(visibleItems.get(position).DateSlt));
        }
    }

    public class BodyViewHolder extends ExpandableRecyclerAdapter.ViewHolder {
        private final LinearLayout slot;
        private final ImageView call;
        private final ImageButton more;
        private TextView tvName,tvTime;

        public BodyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
            slot = (LinearLayout) view.findViewById(R.id.slot);
            call = (ImageView) view.findViewById(R.id.iv_call);
            more = (ImageButton) view.findViewById(R.id.btnMore);
        }

        public void bind(final int position) {
            tvName.setText(visibleItems.get(position).Name);
            tvTime.setText(DateTimeUtility.convertTime24to12Hours(visibleItems.get(position).TimeSlt));
            tvName.setText(visibleItems.get(position).Name);
            tvName.setText(visibleItems.get(position).Name);

            switch (visibleItems.get(position).Status.toLowerCase()){
                case "progress":
                    slot.setBackgroundResource(R.color.status_inprogress);
                    break;
                case "inprocess":
                    slot.setBackgroundResource(R.color.status_process);
                    break;
                case "cancelled":
                    slot.setBackgroundResource(R.color.status_cancelled);
                    break;
                default:
                    slot.setBackgroundResource(R.color.status_pending);
                    break;

            }
            call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(visibleItems.get(position).Status.toLowerCase().equals("progress")) {
                        String phoneNo = visibleItems.get(position).Mobile;
                        AppUser.setActiveMobileNo(mContext,phoneNo);
                        parentClass.callAction(phoneNo);

                    }else if(visibleItems.get(position).Status.toLowerCase().equals("inprocess")) {
                        String time=DateTimeUtility.convertTime24to12Hours(visibleItems.get(position).TimeSlt);
                        String date=DateTimeUtility.convertDateYMD2(visibleItems.get(position).DateSlt);
                        Toast.makeText(mContext, "Call functionality active on date: "+date, Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(mContext, "Slot is cancelled", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String mString=visibleItems.get(position).Status.toLowerCase();
                    switch (mString){
                        case "progress":
                            showPopup(more,visibleItems.get(position),mString);
                            break;
                        case "inprocess":
                            showPopup(more,visibleItems.get(position),mString);
                            break;

                    }
                }
            });
        }
    }

    private void showPopup(ImageButton more, final BodyListItem sample, String mString) {
        final PopupMenu popup = new PopupMenu(mContext, more);
        switch (mString) {
            case "progress":
                popup.getMenuInflater().inflate(R.menu.menu_progress, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        int i = item.getItemId();
                        if (i == R.id.menu_opt_status) {
                            parentClass.updateSlotStatus(sample);
                            return true;
                        } else if (i == R.id.menu_opt_detail) {
                            parentClass.bookingSlotDetail(sample);
                            return true;
                        } else {
                            return onMenuItemClick(item);
                        }
                    }
                });
                popup.show();
                break;
            case "inprocess":
                popup.getMenuInflater().inflate(R.menu.menu_inprocess, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        int i = item.getItemId();
                        if (i == R.id.menu_opt_cancel) {
                            parentClass.cancelSlot(sample);
                            return true;
                        } else if (i == R.id.menu_opt_detail) {
                            parentClass.bookingSlotDetail(sample);
                            return true;
                        } else {
                            return onMenuItemClick(item);
                        }
                    }
                });
                popup.show();
                break;
            case "cancelled":
                more.setVisibility(View.GONE);
                break;

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_HEADER:
                return new HeaderViewHolder(inflate(R.layout.slot_item_seprator, parent));
            case TYPE_BODY:
            default:
                return new BodyViewHolder(inflate(R.layout.slot_bookings, parent));
        }
    }

    @Override
    public void onBindViewHolder(ExpandableRecyclerAdapter.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                ((HeaderViewHolder) holder).bind(position);
                break;
            case TYPE_BODY:
            default:
                ((BodyViewHolder) holder).bind(position);
                break;
        }
    }

    private List<BodyListItem> getSampleItems(ArrayList<BookingSlot> sList) {
        List<BodyListItem> items = new ArrayList<>();
        for (BookingSlot model:sList){
            if(model.getItemType()==BookingSlot.TYPE_SEPARATOR){
                items.add(new BodyListItem(model.getDate()));
            }else {
                items.add(new BodyListItem(model.getUserId(),model.getSlotId(),model.getName(),model.getDate(),
                        model.getTime(),model.getMobile(),model.getDescription(),model.getStatus(),model.getItemType()));
            }

        }
        return items;
    }
}

