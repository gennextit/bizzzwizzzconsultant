package com.gennext.bizzzwizzzconsultant.model;

/**
 * Created by Abhijit on 08-Aug-16.
 */


import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.app.Domains;

import java.util.ArrayList;


public class DomainsAdapter extends ArrayAdapter<DomainsModel> {
    private final ArrayList<DomainsModel> list;
    private final Domains parentClass;

    private Activity context;

    public DomainsAdapter(Activity context, int textViewResourceId, ArrayList<DomainsModel> list, Domains domains) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.parentClass=domains;
        this.list = list;
    }

    class ViewHolder {
        private LinearLayout slotLayout;
        private ImageButton buttonMore;
        private TextView tvDomain;

        public ViewHolder(View v) {
            slotLayout=(LinearLayout)v.findViewById(R.id.slot);
            tvDomain = (TextView) v.findViewById(R.id.tv_domains);
            buttonMore = (ImageButton) v.findViewById(R.id.btnMore);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.slot_domains, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }


        if (list.get(position).getDomainName() != null) {
            holder.tvDomain.setText(list.get(position).getDomainName());
        }
        final ViewHolder finalHolder = holder;
        holder.slotLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(finalHolder,list.get(position).getDomainId(),list.get(position).getDomainName());
            }
        });
        holder.buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(finalHolder,list.get(position).getDomainId(),list.get(position).getDomainName());
            }
        });


        return v;
    }

    private void showPopup(ViewHolder holder, final String domainId, final String domainName) {
        final PopupMenu popup = new PopupMenu(context, holder.buttonMore);
        popup.getMenuInflater().inflate(R.menu.menu_edit, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int i = item.getItemId();
                if (i == R.id.menu_edit_option) {
                    parentClass.editDomain(domainId,domainName);
                    return true;
                }  else {
                    return onMenuItemClick(item);
                }
            }
        });

        popup.show();
    }

}
