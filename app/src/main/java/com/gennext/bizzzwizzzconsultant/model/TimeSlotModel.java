package com.gennext.bizzzwizzzconsultant.model;

import java.util.ArrayList;

/**
 * Created by Abhijit-PC on 22-Feb-17.
 */
public class TimeSlotModel {

    private String slotId;
    private String date;
    private String day;
    private String startTime;
    private String endTime;
    private String Available;

    private String output;
    private String outputMsg;

    public void setFields(String slotId,String date,String day,String startTime,String endTime,String Available){
        this.slotId=slotId;
        this.date=date;
        this.day=day;
        this.startTime=startTime;
        this.endTime=endTime;
        this.Available=Available;
    }

    public String getAvailable() {
        return Available;
    }

    public void setAvailable(String available) {
        Available = available;
    }

    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    private ArrayList<TimeSlotModel>list;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<TimeSlotModel> getList() {
        return list;
    }

    public void setList(ArrayList<TimeSlotModel> list) {
        this.list = list;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
