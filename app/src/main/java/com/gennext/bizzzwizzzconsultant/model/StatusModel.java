package com.gennext.bizzzwizzzconsultant.model;

import java.util.ArrayList;

/**
 * Created by Abhijit-PC on 02-Mar-17.
 */
public class StatusModel {

    private String id;
    private String name;

    private ArrayList<StatusModel>list;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<StatusModel> getList() {
        return list;
    }

    public void setList(ArrayList<StatusModel> list) {
        this.list = list;
    }
}
