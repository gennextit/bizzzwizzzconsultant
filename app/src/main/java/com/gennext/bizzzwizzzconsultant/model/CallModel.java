package com.gennext.bizzzwizzzconsultant.model;

import java.util.ArrayList;

/**
 * Created by Abhijit-PC on 03-Mar-17.
 */
public class CallModel {

    private String Name;
    private String phoneNo;
    private String callType;
    private String callDayTime;
    private String callDuration;

    public void set(String phoneNo,String callType,String callDayTime,String callDuration){
        this.phoneNo=phoneNo;
        this.callType=callType;
        this.callDayTime=callDayTime;
        this.callDuration=callDuration;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    private ArrayList<CallModel>list;

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getCallDayTime() {
        return callDayTime;
    }

    public void setCallDayTime(String callDayTime) {
        this.callDayTime = callDayTime;
    }

    public String getCallDuration() {
        return callDuration;
    }

    public void setCallDuration(String callDuration) {
        this.callDuration = callDuration;
    }

    public ArrayList<CallModel> getList() {
        return list;
    }

    public void setList(ArrayList<CallModel> list) {
        this.list = list;
    }
}
