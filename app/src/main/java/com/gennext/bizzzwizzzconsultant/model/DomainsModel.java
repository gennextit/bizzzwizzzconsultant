package com.gennext.bizzzwizzzconsultant.model;

import com.gennext.bizzzwizzzconsultant.app.Domains;

import java.util.ArrayList;

/**
 * Created by Abhijit-PC on 21-Feb-17.
 */
public class DomainsModel {

    private String domainId;
    private String domainName;

    private String output;
    private String outputMsg;

    public void setFields(String domainId,String domainName){
        this.domainId=domainId;
        this.domainName=domainName;
    }

    private ArrayList<DomainsModel>list;

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<DomainsModel> getList() {
        return list;
    }

    public void setList(ArrayList<DomainsModel> list) {
        this.list = list;
    }
}
