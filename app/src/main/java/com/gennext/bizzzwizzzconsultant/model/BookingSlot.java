package com.gennext.bizzzwizzzconsultant.model;

import java.util.ArrayList;

/**
 * Created by Abhijit-PC on 18-Feb-17.
 */
public class BookingSlot {
    public static final int TYPE_ITEM = 0;
    public static final int TYPE_SEPARATOR = 1;

    private String userId;
    private String slotId;
    private String name;
    private String date;
    private String time;
    private String mobile;
    private String description;
    private String status;
    private int itemType;

    private String output;
    private String outputMsg;

    public void setFields(String userId,String slotId,String name,String date,String time,String mobile,String description
            ,String status,int itemType) {
        this.slotId=slotId;
        this.userId=userId;
        this.name = name;
        this.date=date;
        this.time=time;
        this.mobile=mobile;
        this.description=description;
        this.status=status;
        this.itemType=itemType;
    }


    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public static int getTypeItem() {
        return TYPE_ITEM;
    }

    public static int getTypeSeparator() {
        return TYPE_SEPARATOR;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private ArrayList<BookingSlot> list;

    public void setDateSaprator(String date,int itemType) {
        this.date=date;
        this.itemType=itemType;
    }


    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<BookingSlot> getList() {
        return list;
    }

    public void setList(ArrayList<BookingSlot> list) {
        this.list = list;
    }
}
