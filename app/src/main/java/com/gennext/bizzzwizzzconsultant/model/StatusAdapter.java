package com.gennext.bizzzwizzzconsultant.model;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gennext.bizzzwizzzconsultant.R;

import java.util.ArrayList;

public class StatusAdapter extends BaseAdapter {
    private ArrayList<StatusModel> list;
    private Context context;
    private LayoutInflater inflter;

    public StatusAdapter(Context applicationContext, ArrayList<StatusModel> list) {
        this.context = applicationContext;
        this.list = list;
        inflter = (LayoutInflater.from(applicationContext));
    }

//    @Override
//    public int getCount() {
//        int count = list.size();
//        return count > 0 ? count - 1 : count;
//    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public StatusModel getItem(int i) {
        return list.get(i);
    }

    public String getStatusId(int i) {
        return list.get(i).getId();
    }

    public String getStatusName(int i) {
        return list.get(i).getName();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = inflter.inflate(R.layout.slot_spinner, parent, false);
        TextView names = (TextView) view.findViewById(R.id.tv_message);
        StatusModel model = getItem(position);
        if (position == 0) {
            names.setText("");
            names.setHint(model.getName()); //"Hint to be displayed"
        } else {
            names.setText(model.getName());

        }
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View view;

        if (position == 0) {
            view = inflter.inflate(R.layout.slot_spinner_hint, parent, false); // Hide first row
        } else {
            view = inflter.inflate(R.layout.slot_spinner, parent, false);
            StatusModel model = getItem(position);
            TextView names = (TextView) view.findViewById(R.id.tv_message);
            names.setText(model.getName());
        }

        return view;
    }
}