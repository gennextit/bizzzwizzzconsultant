package com.gennext.bizzzwizzzconsultant.model;

import java.util.ArrayList;

/**
 * Created by Abhijit-PC on 23-Feb-17.
 */
public class WeekDaysModel {

    private String dayId;
    private String dayName;
    private Boolean checked;

    public void setFields(String dayId,String dayName,Boolean checked){
        this.dayId=dayId;
        this.dayName=dayName;
        this.checked=checked;
    }

    private ArrayList<WeekDaysModel>list;

    public ArrayList<WeekDaysModel> getList() {
        return list;
    }

    public void setList(ArrayList<WeekDaysModel> list) {
        this.list = list;
    }

    public String getDayId() {
        return dayId;
    }

    public void setDayId(String dayId) {
        this.dayId = dayId;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }
}
