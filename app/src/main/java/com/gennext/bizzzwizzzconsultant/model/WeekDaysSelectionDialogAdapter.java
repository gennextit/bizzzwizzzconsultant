package com.gennext.bizzzwizzzconsultant.model;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.gennext.bizzzwizzzconsultant.R;

import java.util.ArrayList;

public class WeekDaysSelectionDialogAdapter extends BaseAdapter {

    private static final int START_TIME=1,END_TIME=2;
    private ArrayList<WeekDaysModel> list;
    private Activity context;
    private FragmentManager manager;

    public WeekDaysSelectionDialogAdapter(Activity context, ArrayList<WeekDaysModel> list, FragmentManager manager) {
        this.context = context;
        this.list = list;
        this.manager=manager;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public WeekDaysModel getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public  ArrayList<WeekDaysModel> getList() {
        return list;
    }

    public  ArrayList<WeekDaysModel> getCheckedList() {
        ArrayList<WeekDaysModel>checkList = new ArrayList<>();
        for(WeekDaysModel model:list){
            if(model.getChecked()){
                WeekDaysModel slot=new WeekDaysModel();
                slot.setDayId(model.getDayId());
                slot.setDayName(model.getDayName());
                checkList.add(slot);
            }
        }
        return checkList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.slot_week_days, null, false);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final WeekDaysModel model=list.get(position);
        viewHolder.tvDayName.setText(model.getDayName());

        if(model.getChecked()){
            viewHolder.cbCheck.setChecked(true);
            viewHolder.tvDayName.setBackgroundResource(R.color.dialog_secondary);

        }else {
            viewHolder.cbCheck.setChecked(false);
            viewHolder.tvDayName.setBackgroundResource(R.color.white);
        }
        viewHolder.cbCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getChecked()){
                    list.get(position).setChecked(false);
                    viewHolder.tvDayName.setBackgroundResource(R.color.white);
                }else {
                    list.get(position).setChecked(true);
                    viewHolder.tvDayName.setBackgroundResource(R.color.dialog_secondary);
                }
                notifyDataSetChanged();
            }
        });


        return convertView;
    }



    private static class ViewHolder {
        private final CheckBox cbCheck;
        private TextView tvDayName;

        public ViewHolder(View v) {
            tvDayName = (TextView) v.findViewById(R.id.tv_day);
            cbCheck = (CheckBox) v.findViewById(R.id.checkBox_header);
        }
    }



    private void updateTime(int position, WeekDaysModel timeModel) {
        list.set(position,timeModel);
        notifyDataSetChanged();
    }
}
