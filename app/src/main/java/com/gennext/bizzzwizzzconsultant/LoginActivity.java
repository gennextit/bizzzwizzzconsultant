package com.gennext.bizzzwizzzconsultant;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzzconsultant.common.AppWebView;
import com.gennext.bizzzwizzzconsultant.common.PopupAlert;
import com.gennext.bizzzwizzzconsultant.model.Model;
import com.gennext.bizzzwizzzconsultant.util.ApiCall;
import com.gennext.bizzzwizzzconsultant.util.ApiCallError;
import com.gennext.bizzzwizzzconsultant.util.AppSettings;
import com.gennext.bizzzwizzzconsultant.util.AppUser;
import com.gennext.bizzzwizzzconsultant.util.CompactFragment;
import com.gennext.bizzzwizzzconsultant.util.JsonParser;
import com.gennext.bizzzwizzzconsultant.util.RequestBuilder;


/**
 * Created by Abhijit on 06-Dec-16.
 */

public class LoginActivity extends BaseActivity {

    @Override
    protected String setTitle(Toolbar toolbar) {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public static class AttemptLogin extends CompactFragment implements ApiCallError.ErrorListener {

        private AssignTask assignTask;
        private EditText etUser, etPass;
        private CheckBox cbRememberMe;

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (assignTask != null) {
                assignTask.onAttach(context);
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            if (assignTask != null) {
                assignTask.onDetach();
            }
        }

        public static AttemptLogin newInstance() {
            AttemptLogin fragment = new AttemptLogin();
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View v = inflater.inflate(R.layout.activity_login_screen, container, false);
            InitUI(v);
            setCredentials();
            return v;
        }

        private void setCredentials() {
            String userName = AppUser.getRememberUser(getActivity());
            String pass = AppUser.getRememberPass(getActivity());
            if (!TextUtils.isEmpty(userName)) {
                cbRememberMe.setChecked(true);
                etUser.setText(userName);
                etUser.setSelection(userName.length());
                etPass.setText(pass);
                etPass.setSelection(pass.length());
            } else {
                cbRememberMe.setChecked(false);
            }
        }

        private void InitUI(View v) {
            etUser = (EditText) v.findViewById(R.id.et_signup_username);
            etPass = (EditText) v.findViewById(R.id.et_signup_password);
            LinearLayout llTermsOfUse = (LinearLayout) v.findViewById(R.id.ll_terms_use);
            TextView tvRemember = (TextView) v.findViewById(R.id.tv_rememberme);
            cbRememberMe = (CheckBox) v.findViewById(R.id.cb_login);
            Button btnLogin = (Button) v.findViewById(R.id.btn_mobile_signin);
            Button btnForgotPass = (Button) v.findViewById(R.id.btn_mobile_forgot);
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptLogin();
                }
            });
            tvRemember.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cbRememberMe.isChecked()) {
                        cbRememberMe.setChecked(false);
                    } else {
                        cbRememberMe.setChecked(true);
                    }
                }
            });
            llTermsOfUse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addFragment(AppWebView.newInstance("Terms Of Use", AppSettings.TERMS_OF_USE, AppWebView.PDF), "appWebView");
                }
            });
            btnForgotPass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(getActivity(),ForgotPassActivity.class);
                    intent.putExtra(ForgotPassActivity.TITLE_NAME,"Forgotten account?");
                    startActivity(intent);
                }
            });

        }

        private void attemptLogin() {
            hideKeybord(getActivity());
            assignTask = new AssignTask(getActivity(), etUser.getText().toString(), etPass.getText().toString());
            assignTask.execute(AppSettings.VERIFY_CONSULTANT);
        }

        @Override
        public void onErrorRetryClick(DialogFragment dialog) {
            attemptLogin();
        }

        @Override
        public void onErrorCancelClick(DialogFragment dialog) {

        }

        private class AssignTask extends AsyncTask<String, Void, Model> {
            private String username, password;
            private Context activity;

            public void onAttach(Context activity) {
                // TODO Auto-generated method stub
                this.activity = activity;
            }

            public void onDetach() {
                // TODO Auto-generated method stub
                this.activity = null;
            }

            public AssignTask(Context activity, String username, String password) {
                this.username = username;
                this.password = password;
                this.activity = activity;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressDialog(activity, "Authentication, please wait...");
            }

            @Override
            protected Model doInBackground(String... urls) {
                String response;
                response = ApiCall.POST(urls[0], RequestBuilder.LoginBody(username, password));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseLoginData(activity, response);
            }


            @Override
            protected void onPostExecute(Model result) {
                // TODO Auto-generated method stub
                super.onPostExecute(result);
                if (activity != null) {
                    hideProgressDialog();
                    if (result != null) {
                        if (result.getOutput().equals("success")) {
                            if (cbRememberMe.isChecked()) {
                                AppUser.setRememberUser(activity, username);
                                AppUser.setRememberPass(activity, password);
                            } else {
                                AppUser.setRememberUser(activity, "");
                                AppUser.setRememberPass(activity, "");
                            }
                            AppUser.setAppUserAndPass(activity, username, password);
                            Intent intent = new Intent(activity, MainActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        } else if (result.getOutput().equals("failure")) {
                            PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                    .show(getFragmentManager(), "popupAlert");
                        } else {
                            PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                    .show(getFragmentManager(), "popupAlert");
                        }
                    } else {
                        ApiCallError.newInstance(JsonParser.ERRORMESSAGE, AttemptLogin.this)
                                .show(getFragmentManager(), "apiCallError");

                    }
                }
            }
        }
    }
}
