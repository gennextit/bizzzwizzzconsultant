package com.gennext.bizzzwizzzconsultant.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzzconsultant.R;


public class DomainsEditDialog extends DialogFragment {
    private Dialog dialog;
    private String domainId;
    private String domainName;
    private DialogListener mListener;
    private String domainTitle;

    public interface DialogListener {
        void onDomainUpdateClick(DialogFragment dialog,String domainId,String changedDomain);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static DomainsEditDialog newInstance(String title,String domainId, String domainName, DialogListener listener) {
        DomainsEditDialog fragment = new DomainsEditDialog();
        fragment.domainId = domainId;
        fragment.domainTitle=title;
        fragment.domainName = domainName;
        fragment.mListener = listener;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and domainId
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_domain_edit, null);
        dialogBuilder.setView(v);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        final EditText etDomain = (EditText) v.findViewById(R.id.et_domain);

        tvTitle.setText(domainTitle);
        etDomain.setText(domainName);
        etDomain.setSelection(domainName.length());

        if(domainTitle.contains("Add")){
            button1.setText("Submit");
        }else {
            button1.setText("Update");
        }

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                String domain=etDomain.getText().toString();
                if(!TextUtils.isEmpty(domain)) {
                    dialog.dismiss();
                    if (mListener != null) {
                        mListener.onDomainUpdateClick(DomainsEditDialog.this, domainId, domain);
                    }
                }else{
                    Toast.makeText(getActivity(), "Please enter domain name", Toast.LENGTH_SHORT).show();
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
