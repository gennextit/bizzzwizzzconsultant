package com.gennext.bizzzwizzzconsultant.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.model.BookingSlot;
import com.gennext.bizzzwizzzconsultant.model.DashboardAdapter;


public class SlotFeedback extends DialogFragment {
    private EditText etRemarks;
    private Dialog dialog;
    private String mTitle;
    private SlotFeedbackListener mListener;
    private DashboardAdapter.BodyListItem sample;

    public interface SlotFeedbackListener {
        void onSlotFeedbackOkClick(DialogFragment dialog, DashboardAdapter.BodyListItem sample, String remarks);
    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static SlotFeedback newInstance(String title, DashboardAdapter.BodyListItem sample, SlotFeedbackListener listener) {
        SlotFeedback fragment = new SlotFeedback();
        fragment.mTitle = title;
        fragment.sample = sample;
        fragment.mListener = listener;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_slot_feedback, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        etRemarks = (EditText) v.findViewById(R.id.et_alert_dialog_detail);

        tvTitle.setText(mTitle);
        button1.setText("Submit");
        button2.setText("Cancel");


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onSlotFeedbackOkClick(SlotFeedback.this, sample, etRemarks.getText().toString());
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();

            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }


}
