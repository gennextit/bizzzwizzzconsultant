package com.gennext.bizzzwizzzconsultant.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.StringDef;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.model.TimeSlotModel;
import com.gennext.bizzzwizzzconsultant.util.DateTimeUtility;


public class TimeSlotEditDialog extends DialogFragment implements View.OnClickListener{
    private Dialog dialog;
    private TimeslotEditListener mListener;
    private TimeSlotModel model;
    private TextView tvHour,tvMin,tvAmPm,tvHour2,tvMin2,tvAmPm2;
    private int hour1,min1,hour2,min2;
    private String ampm1,ampm2;


    public interface TimeslotEditListener {
        void onTimeSlotEditClick(DialogFragment dialog, TimeSlotModel model);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static TimeSlotEditDialog newInstance(TimeSlotModel model, TimeslotEditListener listener) {
        TimeSlotEditDialog fragment = new TimeSlotEditDialog();
        fragment.model = model;
        fragment.mListener = listener;
        return fragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_alert_dialog_button1:
                String startTime=DateTimeUtility.convertTimeTo24Hours(hour1,min1,ampm1);
                String endTime=DateTimeUtility.convertTimeTo24Hours(hour2,min2,ampm2);
                if(!TextUtils.isEmpty(startTime)&&!TextUtils.isEmpty(endTime)){
                    int sTime=Integer.parseInt(startTime.replace(":",""));
                    int eTime=Integer.parseInt(endTime.replace(":",""));
                    if(sTime<eTime){
                        dialog.dismiss();
                        if (mListener != null) {
                            model.setStartTime(startTime);
                            model.setEndTime(endTime);
                            mListener.onTimeSlotEditClick(TimeSlotEditDialog.this, model);
                        }
                    }else {
                        Toast.makeText(getActivity(), "Start time must be less than end time", Toast.LENGTH_SHORT).show();
                    }
                }

                break;
            case R.id.btn_alert_dialog_button2:
                dialog.dismiss();
                break;
            case R.id.ib_houre_up:
                tvHour.setText(startHourUp());
                break;
            case R.id.ib_houre_down:
                tvHour.setText(startHourDown());
                break;
            case R.id.ib_min_up:
                tvMin.setText(startMinUp());
                break;
            case R.id.ib_min_down:
                tvMin.setText(startMinDown());
                break;
            case R.id.ib_ampm_up:
                ampm1="AM";
                tvAmPm.setText("AM");
                break;
            case R.id.ib_ampm_down:
                ampm1="PM";
                tvAmPm.setText("PM");
                break;

            case R.id.ib_houre_up2:
                tvHour2.setText(startHourUp2());
                break;
            case R.id.ib_houre_down2:
                tvHour2.setText(startHourDown2());
                break;
            case R.id.ib_min_up2:
                tvMin2.setText(startMinUp2());
                break;
            case R.id.ib_min_down2:
                tvMin2.setText(startMinDown2());
                break;
            case R.id.ib_ampm_up2:
                ampm2="AM";
                tvAmPm2.setText("AM");
                break;
            case R.id.ib_ampm_down2:
                ampm2="PM";
                tvAmPm2.setText("PM");
                break;

        }
    }

    private String startHourUp() {
        if (hour1==12){
            hour1=1;
        }else {
            hour1+=1;
        }
        return String.valueOf(hour1);
    }

    private String startHourDown() {
        if (hour1==1){
            hour1=12;
        }else {
            hour1-=1;
        }
        return String.valueOf(hour1);
    }

    private String startMinUp() {
        if (min1==30){
            min1=0;
        }else {
            min1+=30;
        }
        return String.valueOf(min1);
    }

    private String startMinDown() {
        if (min1==0){
            min1=30;
        }else {
            min1-=30;
        }
        return String.valueOf(min1);
    }

    private String startHourUp2() {
        if (hour2==12){
            hour2=1;
        }else {
            hour2+=1;
        }
        return String.valueOf(hour2);
    }

    private String startHourDown2() {
        if (hour2==1){
            hour2=12;
        }else {
            hour2-=1;
        }
        return String.valueOf(hour2);
    }

    private String startMinUp2() {
        if (min2==30){
            min2=0;
        }else {
            min2+=30;
        }
        return String.valueOf(min2);
    }

    private String startMinDown2() {
        if (min2==0){
            min2=30;
        }else {
            min2-=30;
        }
        return String.valueOf(min2);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and domainId
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_timeslot_edit, null);
        dialogBuilder.setView(v);
        TextView tvDateTimeslot = (TextView) v.findViewById(R.id.tv_date_timeslot);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        ImageButton ibHourUp = (ImageButton) v.findViewById(R.id.ib_houre_up);
        ImageButton ibHourDown = (ImageButton) v.findViewById(R.id.ib_houre_down);
        ImageButton ibMinUp = (ImageButton) v.findViewById(R.id.ib_min_up);
        ImageButton ibMinDown = (ImageButton) v.findViewById(R.id.ib_min_down);
        ImageButton ibAmPmUp = (ImageButton) v.findViewById(R.id.ib_ampm_up);
        ImageButton ibAmPmDown = (ImageButton) v.findViewById(R.id.ib_ampm_down);

        ImageButton ibHourUp2 = (ImageButton) v.findViewById(R.id.ib_houre_up2);
        ImageButton ibHourDown2 = (ImageButton) v.findViewById(R.id.ib_houre_down2);
        ImageButton ibMinUp2 = (ImageButton) v.findViewById(R.id.ib_min_up2);
        ImageButton ibMinDown2 = (ImageButton) v.findViewById(R.id.ib_min_down2);
        ImageButton ibAmPmUp2 = (ImageButton) v.findViewById(R.id.ib_ampm_up2);
        ImageButton ibAmPmDown2 = (ImageButton) v.findViewById(R.id.ib_ampm_down2);

        tvHour= (TextView) v.findViewById(R.id.tv_hour);
        tvMin = (TextView) v.findViewById(R.id.tv_min);
        tvAmPm = (TextView) v.findViewById(R.id.tv_ampm);
        tvHour2= (TextView) v.findViewById(R.id.tv_hour2);
        tvMin2 = (TextView) v.findViewById(R.id.tv_min2);
        tvAmPm2 = (TextView) v.findViewById(R.id.tv_ampm2);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);

        ibHourUp.setOnClickListener(this);
        ibHourDown.setOnClickListener(this);
        ibMinUp.setOnClickListener(this);
        ibMinDown.setOnClickListener(this);
        ibAmPmUp.setOnClickListener(this);
        ibAmPmDown.setOnClickListener(this);

        ibHourUp2.setOnClickListener(this);
        ibHourDown2.setOnClickListener(this);
        ibMinUp2.setOnClickListener(this);
        ibMinDown2.setOnClickListener(this);
        ibAmPmUp2.setOnClickListener(this);
        ibAmPmDown2.setOnClickListener(this);

        tvDateTimeslot.setText(model.getDate()+" ("+model.getDay()+")");
        String[] sTimeArr = DateTimeUtility.splitTime(model.getStartTime());
        String[] eTimeArr = DateTimeUtility.splitTime(model.getEndTime());

        hour1=Integer.parseInt(sTimeArr[0]);
        min1=Integer.parseInt(sTimeArr[1]);
        ampm1=sTimeArr[2];
        hour2=Integer.parseInt(eTimeArr[0]);
        min2=Integer.parseInt(eTimeArr[1]);
        ampm2=eTimeArr[2];

        tvHour.setText(String.valueOf(hour1));
        tvMin.setText(String.valueOf(min1));
        tvAmPm.setText(String.valueOf(ampm1));

        tvHour2.setText(String.valueOf(hour2));
        tvMin2.setText(String.valueOf(min2));
        tvAmPm2.setText(String.valueOf(ampm2));

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
