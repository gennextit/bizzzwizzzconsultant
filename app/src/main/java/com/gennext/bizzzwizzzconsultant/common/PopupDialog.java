package com.gennext.bizzzwizzzconsultant.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.model.DialogModel;


public class PopupDialog extends DialogFragment {
    private TextView tvDescription;
    private Dialog dialog;
    private String mTitle;
    private String mMessage;
    private DialogListener mListener;
    private DialogModel dialogModel;
    private DialogDataListener mDataListener;

    public interface DialogListener {
        void onOkClick(DialogFragment dialog);
        void onCancelClick(DialogFragment dialog);
    }
    public interface DialogDataListener {
        void onOkClick(DialogFragment dialog,DialogModel dialogModel);
        void onCancelClick(DialogFragment dialog,DialogModel dialogModel);
    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static PopupDialog newInstance(String title, String message, DialogListener listener) {
        PopupDialog fragment = new PopupDialog();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.mListener = listener;
        return fragment;
    }

    public static PopupDialog newInstance(String title, String message, DialogModel dialogModel, DialogDataListener listener) {
        PopupDialog fragment = new PopupDialog();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.mDataListener = listener;
        fragment.dialogModel=dialogModel;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);

        tvTitle.setText(mTitle);
        tvDescription.setText(mMessage);
        button1.setText("Ok");
        button2.setText("Cancel");

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onOkClick(PopupDialog.this);
                }
                if (mDataListener != null) {
                    mDataListener.onOkClick(PopupDialog.this,dialogModel);
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onCancelClick(PopupDialog.this);
                }
                if (mDataListener != null) {
                    mDataListener.onCancelClick(PopupDialog.this,dialogModel);
                }
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
