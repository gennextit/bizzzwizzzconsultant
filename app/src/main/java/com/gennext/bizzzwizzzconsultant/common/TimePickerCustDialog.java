package com.gennext.bizzzwizzzconsultant.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.util.DateTimeUtility;


public class TimePickerCustDialog extends DialogFragment implements View.OnClickListener{
    public static final int START_TIME = 1,END_TIME=2;
    private Dialog dialog;
    private TimePickerListener mListener;
    private TextView tvHour,tvMin,tvAmPm;
    private int hour1,min1;
    private String ampm1;
    private String date24h;
    private int timeType;


    public interface TimePickerListener {
        void onTimePickerClick(DialogFragment dialog,int timeType, String  date24h);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static TimePickerCustDialog newInstance(String date24h,int timeType, TimePickerListener listener) {
        TimePickerCustDialog fragment = new TimePickerCustDialog();
        fragment.date24h = date24h;
        fragment.mListener = listener;
        fragment.timeType=timeType;
        return fragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_alert_dialog_button1:
                dialog.dismiss();
                if (mListener != null) {
                    String startTime=DateTimeUtility.convertTimeTo24Hours(hour1,min1,ampm1);
                    date24h=startTime;
                    mListener.onTimePickerClick(TimePickerCustDialog.this,timeType, date24h);
                }

                break;
            case R.id.btn_alert_dialog_button2:
                dialog.dismiss();
                break;
            case R.id.ib_houre_up:
                tvHour.setText(startHourUp());
                break;
            case R.id.ib_houre_down:
                tvHour.setText(startHourDown());
                break;
            case R.id.ib_min_up:
                tvMin.setText(startMinUp());
                break;
            case R.id.ib_min_down:
                tvMin.setText(startMinDown());
                break;
            case R.id.ib_ampm_up:
                ampm1="AM";
                tvAmPm.setText("AM");
                break;
            case R.id.ib_ampm_down:
                ampm1="PM";
                tvAmPm.setText("PM");
                break;
        }
    }

    private String startHourUp() {
        if (hour1==12){
            hour1=1;
        }else {
            hour1+=1;
        }
        return String.valueOf(hour1);
    }

    private String startHourDown() {
        if (hour1==1){
            hour1=12;
        }else {
            hour1-=1;
        }
        return String.valueOf(hour1);
    }

    private String startMinUp() {
        if (min1==30){
            min1=0;
        }else {
            min1+=30;
        }
        return String.valueOf(min1);
    }

    private String startMinDown() {
        if (min1==0){
            min1=30;
        }else {
            min1-=30;
        }
        return String.valueOf(min1);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and domainId
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_time_picker, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        ImageButton ibHourUp = (ImageButton) v.findViewById(R.id.ib_houre_up);
        ImageButton ibHourDown = (ImageButton) v.findViewById(R.id.ib_houre_down);
        ImageButton ibMinUp = (ImageButton) v.findViewById(R.id.ib_min_up);
        ImageButton ibMinDown = (ImageButton) v.findViewById(R.id.ib_min_down);
        ImageButton ibAmPmUp = (ImageButton) v.findViewById(R.id.ib_ampm_up);
        ImageButton ibAmPmDown = (ImageButton) v.findViewById(R.id.ib_ampm_down);


        tvHour= (TextView) v.findViewById(R.id.tv_hour);
        tvMin = (TextView) v.findViewById(R.id.tv_min);
        tvAmPm = (TextView) v.findViewById(R.id.tv_ampm);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);

        ibHourUp.setOnClickListener(this);
        ibHourDown.setOnClickListener(this);
        ibMinUp.setOnClickListener(this);
        ibMinDown.setOnClickListener(this);
        ibAmPmUp.setOnClickListener(this);
        ibAmPmDown.setOnClickListener(this);

        String[] sTimeArr = DateTimeUtility.splitTime(date24h);

        hour1=Integer.parseInt(sTimeArr[0]);
        min1=Integer.parseInt(sTimeArr[1]);
        ampm1=sTimeArr[2];

        tvHour.setText(String.valueOf(hour1));
        tvMin.setText(String.valueOf(min1));
        tvAmPm.setText(String.valueOf(ampm1));

         dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
