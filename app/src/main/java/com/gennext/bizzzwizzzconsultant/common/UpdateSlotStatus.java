package com.gennext.bizzzwizzzconsultant.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.model.BookingSlot;
import com.gennext.bizzzwizzzconsultant.model.DashboardAdapter;
import com.gennext.bizzzwizzzconsultant.model.StatusAdapter;
import com.gennext.bizzzwizzzconsultant.model.StatusModel;
import com.gennext.bizzzwizzzconsultant.util.AppUser;
import com.gennext.bizzzwizzzconsultant.util.JsonParser;


public class UpdateSlotStatus extends DialogFragment {
    private EditText etRemarks;
    private Dialog dialog;
    private String mTitle;
    private UpdateStatusListener mListener;
    private DashboardAdapter.BodyListItem sample;
    private String statusType;
    private Spinner spCallSattus;
    private StatusAdapter statusAdapter;
    private EditText etFeedback;

    public interface UpdateStatusListener {
        void onUpdateSlotOkClick(DialogFragment dialog, DashboardAdapter.BodyListItem sample, String type, String remarks);
    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static UpdateSlotStatus newInstance(String title, DashboardAdapter.BodyListItem sample, UpdateStatusListener listener) {
        UpdateSlotStatus fragment = new UpdateSlotStatus();
        fragment.mTitle = title;
        fragment.sample = sample;
        fragment.mListener = listener;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_update_status, null);
        dialogBuilder.setView(v);
        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        spCallSattus = (Spinner) v.findViewById(R.id.sp_call_status);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        etRemarks = (EditText) v.findViewById(R.id.et_alert_dialog_detail);
        etFeedback = (EditText) v.findViewById(R.id.et_alert_dialog_feedback);

        tvTitle.setText(mTitle);
        button1.setText("Submit");
        button2.setText("Cancel");
        setSpinnerDsta();

        spCallSattus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (statusAdapter != null)
                    statusType = statusAdapter.getStatusId(position);
                String statusName = statusAdapter.getStatusName(position);
                if(statusName.equalsIgnoreCase("call completed")){
                    etFeedback.setVisibility(View.VISIBLE);
                }else{
                    etFeedback.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                if (statusType != null) {
                    dialog.dismiss();
                    if (mListener != null) {
                        mListener.onUpdateSlotOkClick(UpdateSlotStatus.this, sample, statusType, etRemarks.getText().toString());
                    }
                } else {
                    Toast.makeText(getActivity(), "Please select call status", Toast.LENGTH_SHORT).show();
                }

            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();

            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    private void setSpinnerDsta() {
        JsonParser jsonParser = new JsonParser();
        StatusModel model = jsonParser.parseStatusModel(AppUser.getStatusType(getActivity()));
        statusAdapter = new StatusAdapter(getActivity(), model.getList());
        spCallSattus.setAdapter(statusAdapter);
    }
}
