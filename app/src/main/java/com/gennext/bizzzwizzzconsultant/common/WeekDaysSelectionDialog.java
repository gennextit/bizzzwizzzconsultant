package com.gennext.bizzzwizzzconsultant.common;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.model.WeekDaysModel;
import com.gennext.bizzzwizzzconsultant.model.WeekDaysSelectionDialogAdapter;
import com.gennext.bizzzwizzzconsultant.util.JsonParser;

import java.util.ArrayList;


public class WeekDaysSelectionDialog extends DialogFragment {
    private AlertDialog dialog = null;
    private WeekDaysSelectionDialog fragment;
    private WeekDaysSelectionDialogAdapter adapter;
    private ArrayList<WeekDaysModel> originalList;
    private ArrayList<WeekDaysModel> tList;
    private WeekDaysListener mListener;

    public interface WeekDaysListener {
        void onWeekDaysClick(WeekDaysSelectionDialog modeOfPaymentDialog, ArrayList<WeekDaysModel> checkedList, ArrayList<WeekDaysModel> originalList);
    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static WeekDaysSelectionDialog newInstance(WeekDaysListener listener, ArrayList<WeekDaysModel> originalList) {
        WeekDaysSelectionDialog fragment = new WeekDaysSelectionDialog();
        fragment.mListener = listener;
        fragment.fragment = fragment;
        fragment.originalList = originalList;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_week_days, null);
        dialogBuilder.setView(v);
        final CheckBox cbHeader = (CheckBox) v.findViewById(R.id.checkBox_header);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);
        Button btnSubmit = (Button) v.findViewById(R.id.btn_submit);

        tvTitle.setText("Select Days");

        cbHeader.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (cbHeader.isChecked()) {
                    for (WeekDaysModel model : tList) {
                        model.setChecked(true);
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    for (WeekDaysModel model : tList) {
                        model.setChecked(false);
                    }
                    adapter.notifyDataSetChanged();
                }

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (adapter != null) {
                    if (mListener != null) {
                        mListener.onWeekDaysClick(WeekDaysSelectionDialog.this, adapter.getCheckedList(), adapter.getList());
                    }
                    if (fragment != null) {
                        fragment.dismiss();
                    }
                }
            }
        });
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    WeekDaysModel model = adapter.getItem(pos);
                    if (model != null)
                        if (model.getChecked()) {
                            model.setChecked(false);
                            tList.set(pos, model);
                            adapter.notifyDataSetChanged();
                        } else {
                            model.setChecked(true);
                            tList.set(pos, model);
                            adapter.notifyDataSetChanged();
                        }
                }
            }
        });

        if (originalList == null) {
            ArrayList<WeekDaysModel> listM = getAllSelectedDayAndBusinessList();
            if (listM != null) {
                tList = listM;
                adapter = new WeekDaysSelectionDialogAdapter(getActivity(), tList, getFragmentManager());
                lvMain.setAdapter(adapter);
            }
        } else {
            tList = originalList;
            adapter = new WeekDaysSelectionDialogAdapter(getActivity(), tList, getFragmentManager());
            lvMain.setAdapter(adapter);
        }

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    private ArrayList<WeekDaysModel> getAllSelectedDayAndBusinessList() {
        WeekDaysModel allModel = getAllDayAndBusinessList();
        return allModel.getList();
    }

    private WeekDaysModel getAllDayAndBusinessList() {
        return JsonParser.parseDayAndTimingIdJson();
    }


}
