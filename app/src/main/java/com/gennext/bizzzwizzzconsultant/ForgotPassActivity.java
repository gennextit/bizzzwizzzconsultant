package com.gennext.bizzzwizzzconsultant;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.gennext.bizzzwizzzconsultant.app.forgot.ForgotPass;
import com.gennext.bizzzwizzzconsultant.app.forgot.ForgotPassVerify;
import com.gennext.bizzzwizzzconsultant.app.forgot.GenPassword;

/**
 * Created by Abhijit-PC on 15-Mar-17.
 */

public class ForgotPassActivity extends BaseActivity{
    public static final int EMAIL = 11, MOBILE = 12;
    public static final String TITLE_NAME = "titleName";
    private String title;

    @Override
    protected String setTitle(Toolbar toolbar) {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title=getIntent().getStringExtra(TITLE_NAME);
        setForgotPassScreen();
    }

    private void setForgotPassScreen() {
        addFragmentWithoutBackstack(ForgotPass.newInstance(ForgotPassActivity.this,title),"forgotPass");
    }

    public void setForgotPassVerifyScreen(int verifyType, String input) {
        addFragmentWithoutBackstack(ForgotPassVerify.newInstance(ForgotPassActivity.this,title,verifyType,input),"forgotPassVerify");
    }

    public void setForgotPassCreateScreen(int verifyType, String input) {
        addFragmentWithoutBackstack(GenPassword.newInstance(ForgotPassActivity.this,title,verifyType,input),"genPassword");
    }

//    private void setScreen(int type) {
//        switch (type){
//            case FORGOT_PASS:
//                break;
//            case FORGOT_PASS_VERIFY:
//                break;
//            case FORGOT_PASS_CREATE:
//                break;
//        }
//    }
}
