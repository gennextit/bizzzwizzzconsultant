package com.gennext.bizzzwizzzconsultant.app;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.common.DatePickerDialog;
import com.gennext.bizzzwizzzconsultant.common.PopupAlert;
import com.gennext.bizzzwizzzconsultant.common.TimePickerCustDialog;
import com.gennext.bizzzwizzzconsultant.common.WeekDaysSelectionDialog;
import com.gennext.bizzzwizzzconsultant.model.ApiErrorModel;
import com.gennext.bizzzwizzzconsultant.model.Model;
import com.gennext.bizzzwizzzconsultant.model.WeekDaysModel;
import com.gennext.bizzzwizzzconsultant.util.ApiCall;
import com.gennext.bizzzwizzzconsultant.util.ApiCallError;
import com.gennext.bizzzwizzzconsultant.util.AppAnimation;
import com.gennext.bizzzwizzzconsultant.util.AppSettings;
import com.gennext.bizzzwizzzconsultant.util.AppUser;
import com.gennext.bizzzwizzzconsultant.util.CompactFragment;
import com.gennext.bizzzwizzzconsultant.util.DateTimeUtility;
import com.gennext.bizzzwizzzconsultant.util.JsonParser;
import com.gennext.bizzzwizzzconsultant.util.RequestBuilder;

import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by Abhijit-PC on 18-Feb-17.
 */

public class AddTimeSlot extends CompactFragment implements ApiCallError.ErrorFlagListener,WeekDaysSelectionDialog.WeekDaysListener,TimePickerCustDialog.TimePickerListener,View.OnClickListener,DatePickerDialog.DateSelectFlagListener{

    private int sday, smonth, syear;
    private int eday, emonth, eyear;
    private String sDate,eDate;
    private String sTime,eTime;
    private int startDateTimeStamp,endDateTimeStamp;
    private Button btnStartDate,btnEndDate,btnStartTime,btnEndTime,btnSelectDays;
    private ArrayList<WeekDaysModel> daysSelectionList;
    private ArrayList<WeekDaysModel> sltDaysList;

    private AssignTask assignTask;
    private String sltDaysIds;
    private ViewTimeSlot viewTimeSlot;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static AddTimeSlot newInstance(Context activity, ViewTimeSlot viewTimeSlot) {
        AddTimeSlot fragment = new AddTimeSlot();
        fragment.viewTimeSlot=viewTimeSlot;
        AppAnimation.setDrawerAnimation(activity,fragment);
        return fragment;
    }

    public void getDateTime() {
        final Calendar cal = Calendar.getInstance();
        if(sDate==null) {
            sTime="10:30";
            btnStartTime.setText(DateTimeUtility.convertTime24to12Hours(sTime));

            syear = cal.get(Calendar.YEAR);
            smonth = cal.get(Calendar.MONTH);
            sday = cal.get(Calendar.DAY_OF_MONTH);
            sDate=DateTimeUtility.cDateDDMMMYY(sday,smonth,syear);
            startDateTimeStamp=DateTimeUtility.generateTimeStamp(syear,smonth,sday);
            btnStartDate.setText(sDate);
        }else {
            String date=DateTimeUtility.convertDateSplit(sDate);
            String[] temp = date.split("-");
            sday=Integer.parseInt(temp[0]);
            int tempMonth = Integer.parseInt(temp[1]);
            smonth=tempMonth-1;
            syear=Integer.parseInt(temp[2]);
            btnStartDate.setText(sDate);
            startDateTimeStamp=DateTimeUtility.generateTimeStamp(syear,smonth,sday);
        }
        if(eDate==null) {
            eTime="18:30";
            btnEndTime.setText(DateTimeUtility.convertTime24to12Hours(eTime));
            eyear = cal.get(Calendar.YEAR);
            emonth = cal.get(Calendar.MONTH);
            eday = cal.get(Calendar.DAY_OF_MONTH);
            eDate=DateTimeUtility.cDateDDMMMYY(eday,emonth,eyear);
            btnEndDate.setText(eDate);
            endDateTimeStamp=DateTimeUtility.generateTimeStamp(eyear,emonth,eday);
        }else {
            String date=DateTimeUtility.convertDateSplit(eDate);
            String[] temp = date.split("-");
            eday=Integer.parseInt(temp[0]);
            int tempMonth = Integer.parseInt(temp[1]);
            emonth=tempMonth-1;
            eyear=Integer.parseInt(temp[2]);
            btnEndDate.setText(eDate);
            endDateTimeStamp=DateTimeUtility.generateTimeStamp(eyear,emonth,eday);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_timeslot_add, container, false);
        initToolBar(getActivity(),v,"Add Time Slot");
        InitUI(v);
        getDateTime();
        return v;
    }

    private void InitUI(View v) {
        btnStartDate=(Button)v.findViewById(R.id.btn_start_date);
        btnEndDate=(Button)v.findViewById(R.id.btn_end_date);
        btnStartTime=(Button)v.findViewById(R.id.btn_start_time);
        btnEndTime=(Button)v.findViewById(R.id.btn_end_time);
        btnSelectDays=(Button)v.findViewById(R.id.btn_select_days);
        Button btnSubmit=(Button)v.findViewById(R.id.btn_submit);

        btnStartDate.setOnClickListener(this);
        btnEndDate.setOnClickListener(this);
        btnStartTime.setOnClickListener(this);
        btnEndTime.setOnClickListener(this);
        btnSelectDays.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start_date:
                DatePickerDialog.newInstance(AddTimeSlot.this,true,DatePickerDialog.START_DATE,sday,smonth,syear)
                        .show(getFragmentManager(),"datePickerDialog");
                break;
            case R.id.btn_end_date:
                DatePickerDialog.newInstance(AddTimeSlot.this,true,DatePickerDialog.END_DATE,eday,emonth,eyear)
                        .show(getFragmentManager(),"datePickerDialog");
                break;
            case R.id.btn_start_time:
                TimePickerCustDialog.newInstance(sTime,TimePickerCustDialog.START_TIME,AddTimeSlot.this)
                        .show(getFragmentManager(),"timePickerCustDialog");
                break;
            case R.id.btn_end_time:
                TimePickerCustDialog.newInstance(eTime,TimePickerCustDialog.END_TIME,AddTimeSlot.this)
                        .show(getFragmentManager(),"timePickerCustDialog");
                break;
            case R.id.btn_select_days:
                showDaysSelection();
                break;
            case R.id.btn_submit:
                validateAndExecute();
                break;
        }
    }

    private void showDaysSelection() {
        WeekDaysSelectionDialog.newInstance(AddTimeSlot.this,daysSelectionList)
                .show(getFragmentManager(),"WeekDaysSelectionDialog");
    }

    private void validateAndExecute() {
        int stTime=Integer.parseInt(sTime.replace(":",""));
        int etTime=Integer.parseInt(eTime.replace(":",""));

        if(startDateTimeStamp>endDateTimeStamp){
            Toast.makeText(getActivity(), "Start date must be less than end date.", Toast.LENGTH_SHORT).show();
        }if(stTime>etTime){
            Toast.makeText(getActivity(), "Start time must be less than end time.", Toast.LENGTH_SHORT).show();
        }else{
            if(sltDaysIds==null){
                showDaysSelection();
                Toast.makeText(getActivity(), "Please select days", Toast.LENGTH_SHORT).show();
            }else {
                executeTask(sDate, eDate, sTime, eTime, sltDaysIds);
            }
        }
    }

    @Override
    public void onSelectDateFlagClick(DialogFragment dialog, int dateType, int date, int month, int year) {
        if (dateType == DatePickerDialog.START_DATE) {
            sday = date;
            smonth = month;
            syear = year;
            sDate=DateTimeUtility.cDateDDMMMYY(date,month,year);
            startDateTimeStamp=DateTimeUtility.generateTimeStamp(syear,smonth,sday);
            btnStartDate.setText(sDate);
        } else {
            eday = date;
            emonth = month;
            eyear = year;
            eDate=DateTimeUtility.cDateDDMMMYY(date,month,year);
            btnEndDate.setText(eDate);
            endDateTimeStamp=DateTimeUtility.generateTimeStamp(eyear,emonth,eday);
        }
    }

    @Override
    public void onTimePickerClick(DialogFragment dialog, int timeType, String date24h) {
        if(timeType==TimePickerCustDialog.START_TIME){
            sTime=date24h;
            btnStartTime.setText(DateTimeUtility.convertTime24to12Hours(sTime));
        }else if(timeType==TimePickerCustDialog.END_TIME){
            eTime=date24h;
            btnEndTime.setText(DateTimeUtility.convertTime24to12Hours(eTime));
        }
    }

    @Override
    public void onWeekDaysClick(WeekDaysSelectionDialog modeOfPaymentDialog, ArrayList<WeekDaysModel> checkedList, ArrayList<WeekDaysModel> originalList) {
        this.daysSelectionList=originalList;
        this.sltDaysList = checkedList;
        String sltDay = "",sltDayIds = "";
        if (sltDaysList != null && sltDaysList.size() != 0) {
            for (WeekDaysModel model : sltDaysList) {
                sltDay += model.getDayName() + ",";
                sltDayIds+=model.getDayName() +",";
            }
            String sltDayAndTime =sltDaysIds= sltDay.substring(0, sltDay.length() - 1);
//            this.sltDaysIds=sltdayids.substring(0, sltDayIds.length() - 1);
            btnSelectDays.setText(sltDayAndTime);
        }else{
            this.sltDaysIds=null;
            btnSelectDays.setText("Select Days");
        }
    }

    private void executeTask(String startDate,String endDate,String startTime,String endTime,String sltDaysId) {
        //consultantId,startDate,endDate,startTime,endTime,days
        assignTask = new AssignTask(getActivity(),startDate,endDate,startTime,endTime,sltDaysId);
        assignTask.execute(AppSettings.ADD_TIME_SLOT);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, ApiErrorModel model) {
        executeTask(model.getValue1(),model.getValue2(),model.getValue3(),model.getValue4(),model.getValue5());
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String startDate,endDate,startTime,endTime,sltDaysId;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context,String startDate,String endDate,String startTime,String endTime
                ,String sltDaysId) {
            this.startDate=startDate;
            this.endDate=endDate;
            this.startTime=startTime;
            this.endTime=endTime;
            this.sltDaysId=sltDaysId;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context,"Processing please wait.");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            String consultantId= AppUser.getConsultantId(context);
            response = ApiCall.POST(urls[0], RequestBuilder.AddTimeSlot(consultantId,DateTimeUtility.serverFormat(startDate),
                    DateTimeUtility.serverFormat(endDate),startTime,endTime,sltDaysIds));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.defaultParser(response);
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if(viewTimeSlot!=null) {
                            viewTimeSlot.executeTask(ViewTimeSlot.TASK_LOADLIST);
                            PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT)
                                    .show(getFragmentManager(), "popupAlert");
                        }else{
                            Toast.makeText(context, result.getOutputMsg(), Toast.LENGTH_LONG).show();
                            getFragmentManager().popBackStack();
                            addFragment(ViewTimeSlot.newInstance(getActivity()),"viewTimeSlot");
                        }
                    } else if (result.getOutput().equals("failure")) {
                        PopupAlert.newInstance("Alert",result.getOutputMsg(),PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(),"popupAlert");
                    } else {
                        PopupAlert.newInstance("Alert",result.getOutputMsg(),PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(),"popupAlert");
                    }
                }else{
                    ApiErrorModel model=new ApiErrorModel();
                    model.setFields(startDate,endDate,startTime,endTime,sltDaysId);
                    ApiCallError.newInstance(JsonParser.ERRORMESSAGE,model,AddTimeSlot.this)
                            .show(getFragmentManager(),"apiCallError");
                }
            }
        }
    }
}
