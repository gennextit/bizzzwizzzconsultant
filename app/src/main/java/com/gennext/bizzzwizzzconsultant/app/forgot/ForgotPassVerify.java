package com.gennext.bizzzwizzzconsultant.app.forgot;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gennext.bizzzwizzzconsultant.ForgotPassActivity;
import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.common.PopupAlert;
import com.gennext.bizzzwizzzconsultant.model.ApiErrorModel;
import com.gennext.bizzzwizzzconsultant.model.Model;
import com.gennext.bizzzwizzzconsultant.util.ApiCall;
import com.gennext.bizzzwizzzconsultant.util.ApiCallError;
import com.gennext.bizzzwizzzconsultant.util.AppAnimation;
import com.gennext.bizzzwizzzconsultant.util.AppSettings;
import com.gennext.bizzzwizzzconsultant.util.CompactFragment;
import com.gennext.bizzzwizzzconsultant.util.JsonParser;
import com.gennext.bizzzwizzzconsultant.util.RequestBuilder;

import java.util.concurrent.TimeUnit;


/**
 * Created by Abhijit on 14-Oct-16.
 */

public class ForgotPassVerify extends CompactFragment implements ApiCallError.ErrorFlagListener {
    private static final int VERIFY = 3, RESEND_OTP = 4;
    private Button btnOK;
    AssignTask assignTask;
    private EditText etVerifyOtp;
    private long timeInMSec = 60000;
    private TextView tvTimerCount;
    private Button btnResendOtp;
    private String input;
    private int verifyType;
    private String title;

    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static ForgotPassVerify newInstance(Activity act, String title, int verifyType, String input) {
        ForgotPassVerify fragment = new ForgotPassVerify();
        fragment.title = title;
        fragment.input = input;
        fragment.verifyType = verifyType;
        AppAnimation.setDialogAnimation(act, fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_forgot_pass_verify, container, false);
        initToolBarForActivity(getActivity(),v,title);
        InitUI(v);
        startCountdownTimer();
        return v;
    }

    private void startCountdownTimer() {
        CounterClass timer = new CounterClass(timeInMSec, 1000);
        timer.start();
    }

    private void InitUI(View v) {
        btnOK = (Button) v.findViewById(R.id.btn_submit);
        etVerifyOtp = (EditText) v.findViewById(R.id.et_verify_otp);
        tvTimerCount = (TextView) v.findViewById(R.id.tv_timer_count);
        btnResendOtp = (Button) v.findViewById(R.id.btn_mobile_retry);

        if (verifyType == ForgotPassActivity.MOBILE) {
            etVerifyOtp.setHint("Enter otp received through sms");
        } else if (verifyType ==ForgotPassActivity.EMAIL) {
            etVerifyOtp.setHint("Enter otp received through email");
        }

        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                executeTask(TASK_RESEND_OTP);
            }
        });



        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                executeTask(VERIFY);
            }
        });
    }

    private void executeTask(int task) {
        if (verifyType == ForgotPassActivity.MOBILE) {
            if (task == VERIFY) {
                assignTask = new AssignTask(getActivity(), task, input, etVerifyOtp.getText().toString());
                assignTask.execute(AppSettings.VERIFY_MOBILE_OTP);
            } else if (task == RESEND_OTP) {
                assignTask = new AssignTask(getActivity(), task, input, null);
                assignTask.execute(AppSettings.VERIFY_MOBILE);
            }
        } else if (verifyType == ForgotPassActivity.EMAIL) {
            if (task == VERIFY) {
                assignTask = new AssignTask(getActivity(), task, input, etVerifyOtp.getText().toString());
                assignTask.execute(AppSettings.VERIFY_EMAIL);
            } else if (task == RESEND_OTP) {
                assignTask = new AssignTask(getActivity(), task, input, null);
                assignTask.execute(AppSettings.VERIFY_EMAIL_OTP);
            }
        }

    }



    @Override
    public void onErrorRetryClick(DialogFragment dialog, ApiErrorModel apiModel) {
        executeTask(apiModel.getTask());
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String input, otp;
        private Context activity;
        private int task;

        public void onAttach(Context activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, int task, String input, String otp) {
            this.task = task;
            this.input = input;
            this.otp = otp;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Processing please wait...");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            if (verifyType == ForgotPassActivity.MOBILE) {
                if (task == VERIFY) {
                    response = ApiCall.POST(urls[0], RequestBuilder.verifyOtpMobile(input,otp));
                    JsonParser jsonParser = new JsonParser();
                    return jsonParser.defaultParser(response);
                } else if (task == RESEND_OTP) {
                    response = ApiCall.POST(urls[0], RequestBuilder.verifyMobile(input));
                    JsonParser jsonParser = new JsonParser();
                    return jsonParser.defaultParser(response);
                }
            } else if (verifyType == ForgotPassActivity.EMAIL) {
                if (task == VERIFY) {
                    response = ApiCall.POST(urls[0], RequestBuilder.verifyOtpEmail(input,otp));
                    JsonParser jsonParser = new JsonParser();
                    return jsonParser.defaultParser(response);
                } else if (task == RESEND_OTP) {
                    response = ApiCall.POST(urls[0], RequestBuilder.verifyEmail(input));
                    JsonParser jsonParser = new JsonParser();
                    return jsonParser.defaultParser(response);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (task == VERIFY) {
                            getFragmentManager().popBackStack();
                            ((ForgotPassActivity)getActivity()).setForgotPassCreateScreen(verifyType,input);
                        } else if (task == RESEND_OTP) {
                            tvTimerCount.setVisibility(View.VISIBLE);
                            btnResendOtp.setVisibility(View.GONE);
                            startCountdownTimer();
                        }
//                        if (verifyType == ForgotPassActivity.MOBILE) {
//                            if (task == VERIFY) {
//                                getFragmentManager().popBackStack();
//                                ((ForgotPassActivity)getActivity()).setForgotPassCreateScreen(verifyType,input);
//                            } else if (task == RESEND_OTP) {
//                                tvTimerCount.setVisibility(View.VISIBLE);
//                                btnResendOtp.setVisibility(View.GONE);
//                                startCountdownTimer();
//                            }
//                        } else if (verifyType == ForgotPassActivity.EMAIL) {
//                            if (task == VERIFY) {
//                                getFragmentManager().popBackStack();
//                                ((ForgotPassActivity)getActivity()).setForgotPassCreateScreen(verifyType,input);
//                            } else if (task == RESEND_OTP) {
//                                tvTimerCount.setVisibility(View.VISIBLE);
//                                btnResendOtp.setVisibility(View.GONE);
//                                startCountdownTimer();
//                            }
//                        }

                    } else if (result.getOutput().equals("failure")) {
                        PopupAlert.newInstance("Feedback", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    }
                } else {
                    ApiErrorModel errorModel=new ApiErrorModel();
                    errorModel.setTask(task);
                    ApiCallError.newInstance(JsonParser.ERRORMESSAGE,errorModel,ForgotPassVerify.this)
                            .show(getFragmentManager(), "apiCallError");

                }
            }
        }
    }

    public class CounterClass extends CountDownTimer {
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            tvTimerCount.setVisibility(View.GONE);
            btnResendOtp.setVisibility(View.VISIBLE);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
//            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
//                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
//                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            String hms = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));


//            long res=(millis *100)/timeInMSec;

            tvTimerCount.setText("Wait " + hms.substring(3, 5) + " sec");
//            arcProgress.setProgress((int)res);
//            arcProgress.setBottomText(hms);

        }

    }
}