package com.gennext.bizzzwizzzconsultant.app.forgot;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gennext.bizzzwizzzconsultant.ForgotPassActivity;
import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.common.PopupAlert;
import com.gennext.bizzzwizzzconsultant.model.Model;
import com.gennext.bizzzwizzzconsultant.util.ApiCall;
import com.gennext.bizzzwizzzconsultant.util.ApiCallError;
import com.gennext.bizzzwizzzconsultant.util.AppAnimation;
import com.gennext.bizzzwizzzconsultant.util.AppSettings;
import com.gennext.bizzzwizzzconsultant.util.CompactFragment;
import com.gennext.bizzzwizzzconsultant.util.FieldValidation;
import com.gennext.bizzzwizzzconsultant.util.JsonParser;
import com.gennext.bizzzwizzzconsultant.util.RequestBuilder;


/**
 * Created by Abhijit on 14-Oct-16.
 */

public class ForgotPass extends CompactFragment implements ApiCallError.ErrorListener{
    private Button btnOK;
    AssignTask assignTask;
    private EditText etMobile,etEmail;
    private String title;

    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static ForgotPass newInstance(Activity act, String title) {
        ForgotPass forgotPass=new ForgotPass();
        forgotPass.title=title;
        AppAnimation.setDialogAnimation(act,forgotPass);
        return forgotPass;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_forgot_pass, container, false);
        initToolBarForActivity(getActivity(),v,title);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        btnOK = (Button) v.findViewById(R.id.btn_submit);
        etMobile = (EditText) v.findViewById(R.id.et_verify_mobile);
        etEmail = (EditText) v.findViewById(R.id.et_verify_email);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                executeTask();
            }
        });
    }

    private void executeTask() {
       if(checkValidation()) {
           if(!etMobile.getText().toString().equals("")) {
               assignTask = new AssignTask(getActivity(), ForgotPassActivity.MOBILE, etMobile.getText().toString());
               assignTask.execute(AppSettings.VERIFY_MOBILE);
           }else if(!etEmail.getText().toString().equals("")) {
               assignTask = new AssignTask(getActivity(), ForgotPassActivity.EMAIL, etEmail.getText().toString());
               assignTask.execute(AppSettings.VERIFY_EMAIL);
           }
        }
    }

    private boolean checkValidation() {
        String mobile=etMobile.getText().toString();
        String email=etEmail.getText().toString();
        if (!mobile.equals("")&&!email.equals("")) {
            Toast.makeText(getActivity(), "Please input any one of them.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (FieldValidation.validateOnly(getActivity(), etMobile, FieldValidation.MOBILE, true)||FieldValidation.validateOnly(getActivity(), etEmail, FieldValidation.EMAIL, true)) {
            return true;
        }
        return false;
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final int verifyType;
        private Context activity;
        private String input;

        public void onAttach(Context activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, int verifyType, String input) {
            this.verifyType = verifyType;
            this.input = input;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Processing please wait...");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response="";
            if(verifyType==ForgotPassActivity.MOBILE) {
                response = ApiCall.POST(urls[0], RequestBuilder.verifyMobile(input));
            }else{
                response = ApiCall.POST(urls[0], RequestBuilder.verifyEmail(input));
            }
            JsonParser jsonParser = new JsonParser();
            return jsonParser.defaultParser(response);
        }

        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        getFragmentManager().popBackStack();
                        ((ForgotPassActivity)getActivity()).setForgotPassVerifyScreen(verifyType,input);
                    } else if (result.getOutput().equals("failure")) {
                        PopupAlert.newInstance("Feedback",result.getOutputMsg(),PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(),"popupAlert");
                    } else {
                        PopupAlert.newInstance("Alert",result.getOutputMsg(),PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(),"popupAlert");
                    }
                } else {
                    ApiCallError.newInstance(JsonParser.ERRORMESSAGE,ForgotPass.this)
                            .show(getFragmentManager(),"apiCallError");

                }
            }
        }
    }
}