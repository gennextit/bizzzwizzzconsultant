package com.gennext.bizzzwizzzconsultant.app;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.common.DomainsEditDialog;
import com.gennext.bizzzwizzzconsultant.common.PopupAlert;
import com.gennext.bizzzwizzzconsultant.common.PopupDialog;
import com.gennext.bizzzwizzzconsultant.model.ApiErrorModel;
import com.gennext.bizzzwizzzconsultant.model.DialogModel;
import com.gennext.bizzzwizzzconsultant.model.DomainsAdapter;
import com.gennext.bizzzwizzzconsultant.model.DomainsModel;
import com.gennext.bizzzwizzzconsultant.util.ApiCall;
import com.gennext.bizzzwizzzconsultant.util.ApiCallError;
import com.gennext.bizzzwizzzconsultant.util.AppAnimation;
import com.gennext.bizzzwizzzconsultant.util.AppSettings;
import com.gennext.bizzzwizzzconsultant.util.AppUser;
import com.gennext.bizzzwizzzconsultant.util.CompactFragment;
import com.gennext.bizzzwizzzconsultant.util.JsonParser;
import com.gennext.bizzzwizzzconsultant.util.RequestBuilder;


/**
 * Created by Abhijit-PC on 18-Feb-17.
 */

public class Domains extends CompactFragment implements ApiCallError.ErrorFlagListener,DomainsEditDialog.DialogListener,PopupDialog.DialogDataListener{

    private ListView lvMain;
    private DomainsAdapter adapter;
    private AssignTask assignTask;
    private static final int TASK_LOADLIST=1,TASK_UPDATE_DOMAIN=2;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static Domains newInstance(Activity act) {
        Domains fragment = new Domains();
        AppAnimation.setDialogAnimation(act,fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_domains, container, false);
        initToolBar(getActivity(),v,"Domains");
        InitUI(v);
        executeTask(TASK_LOADLIST, null,null);
        return v;
    }

    private void InitUI(View v) {
        lvMain=(ListView)v.findViewById(R.id.lv_main);
        final ImageView toolbarMenu = (ImageView) v.findViewById(R.id.iv_menu);
        toolbarMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(toolbarMenu);
            }
        });
    }

    public void editDomain(String domainId, String domainName) {
        DomainsEditDialog.newInstance("Edit Domain",domainId,domainName,Domains.this)
                .show(getFragmentManager(),"domainsEditDialog");
    }

    private void showPopup(ImageView imageView) {
        final PopupMenu popup = new PopupMenu(getActivity(), imageView);
        popup.getMenuInflater().inflate(R.menu.add_domain, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int i = item.getItemId();
                if (i == R.id.menu_add_domain_option) {
                    DomainsEditDialog.newInstance("Add Domain","","",Domains.this)
                            .show(getFragmentManager(),"domainsEditDialog");
                    return true;
                }  else {
                    return onMenuItemClick(item);
                }
            }
        });
        popup.show();
    }

    @Override
    public void onDomainUpdateClick(DialogFragment dialog, String domainId, String changedDomain) {
        DialogModel dialogModel=new DialogModel();
        dialogModel.setValue1(domainId);
        dialogModel.setValue2(changedDomain);
        PopupDialog.newInstance("Alert","Are you sure to update this domain\n"+changedDomain,dialogModel,Domains.this)
                .show(getFragmentManager(),"popupDialog");
    }

    @Override
    public void onOkClick(DialogFragment dialog, DialogModel dialogModel) {
        executeTask(TASK_UPDATE_DOMAIN,dialogModel.getValue1(),dialogModel.getValue2());
    }

    @Override
    public void onCancelClick(DialogFragment dialog, DialogModel dialogModel) {

    }

    private void executeTask(int task, String domainId, String domainName) {
        if (task==TASK_LOADLIST) {
            assignTask = new AssignTask(getActivity(),task,null,null);
            assignTask.execute(AppSettings.LOAD_DOMAIN);
        } else if(task==TASK_UPDATE_DOMAIN){
            assignTask = new AssignTask(getActivity(),task,domainId,domainName);
            assignTask.execute(AppSettings.UPDATE_DOMAIN);
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, ApiErrorModel model) {
        executeTask(model.getTask(), model.getValue1(), model.getValue2());
    }


    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    private class AssignTask extends AsyncTask<String, Void, DomainsModel> {

        private int task;
        private String domainId,domainName;
        private Context context;
        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, int task,String domainId,String domainName) {
            this.domainId=domainId;
            this.domainName=domainName;
            this.context = context;
            this.task=task;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (task==TASK_LOADLIST) {
                showProgressDialog(context,"Loading please wait.");
            } else {
                showProgressDialog(context,"Updating data please wait.");
            }
        }

        @Override
        protected DomainsModel doInBackground(String... urls) {
            if (task==TASK_LOADLIST) {
                JsonParser jsonParser=new JsonParser();
                return jsonParser.parseDomainData();
            } else {
                String response;
                String userId= AppUser.getConsultantId(context);
                response = ApiCall.POST(urls[0], RequestBuilder.UpdateDomain(userId,domainId,domainName));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseUpdateDomain(response);
            }
        }

        @Override
        protected void onPostExecute(DomainsModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (task==TASK_LOADLIST) {
                            adapter = new DomainsAdapter(getActivity(),R.layout.slot_bookings, result.getList(),Domains.this);
                            lvMain.setAdapter(adapter);
                        } else if(task==TASK_UPDATE_DOMAIN){
                            executeTask(TASK_LOADLIST,null,null);
                        }
                    } else if (result.getOutput().equals("failure")) {
                        PopupAlert.newInstance("Alert",result.getOutputMsg(),PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(),"popupAlert");
                    } else {
                        PopupAlert.newInstance("Alert",result.getOutputMsg(),PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(),"popupAlert");
                    }
                }else{
                    ApiErrorModel apiErrorModel=new ApiErrorModel();
                    apiErrorModel.setTask(task);
                    apiErrorModel.setValue1(domainId);
                    apiErrorModel.setValue2(domainName);
                    ApiCallError.newInstance(JsonParser.ERRORMESSAGE,apiErrorModel,Domains.this)
                            .show(getFragmentManager(),"apiCallError");
                }
            }
        }

    }
}
