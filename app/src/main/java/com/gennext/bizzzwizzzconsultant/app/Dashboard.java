package com.gennext.bizzzwizzzconsultant.app;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.common.PopupAlert;
import com.gennext.bizzzwizzzconsultant.common.SlotFeedback;
import com.gennext.bizzzwizzzconsultant.common.UpdateSlotStatus;
import com.gennext.bizzzwizzzconsultant.model.ApiErrorModel;
import com.gennext.bizzzwizzzconsultant.model.BookingSlot;
import com.gennext.bizzzwizzzconsultant.model.DashboardAdapter;
import com.gennext.bizzzwizzzconsultant.util.ApiCall;
import com.gennext.bizzzwizzzconsultant.util.ApiCallError;
import com.gennext.bizzzwizzzconsultant.util.AppSettings;
import com.gennext.bizzzwizzzconsultant.util.AppUser;
import com.gennext.bizzzwizzzconsultant.util.CompactFragment;
import com.gennext.bizzzwizzzconsultant.util.JsonParser;
import com.gennext.bizzzwizzzconsultant.util.RequestBuilder;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;

import java.util.ArrayList;


/**
 * Created by Abhijit-PC on 18-Feb-17.
 */

public class Dashboard extends CompactFragment implements SlotFeedback.SlotFeedbackListener,UpdateSlotStatus.UpdateStatusListener, ApiCallError.ErrorFlagListener {

    private static final int TASK_LOADLIST = 1, TASK_STATUS_UPDATE = 2, TASK_CANCEL = 3,TASK_FEEDBACK=4;
//    private ListView lvMain;
//    private BookingSlotAdapter adapter2;

    private AssignTask assignTask;
    private TextView tvName;
    private TextView tvUserId;
    private LinearLayout menu1,menu2;
    private DashboardAdapter adapter;
    private RecyclerView recycler;
    private ArrayList<BookingSlot> sList;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Dashboard newInstance() {
        Dashboard fragment = new Dashboard();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_dashboard, container, false);
        InitUI(v);
        setUserNameAndId();
        executeTask(TASK_LOADLIST);
        return v;
    }

    private void setUserNameAndId() {
        String userId=AppUser.getUserId(getActivity());
        String name=AppUser.getName(getActivity());
        tvName.setText(name);
        tvUserId.setText(userId);
    }

    private void InitUI(View v) {
//        lvMain = (ListView) v.findViewById(R.id.lv_main);
        tvName = (TextView) v.findViewById(R.id.tv_name);
        tvUserId = (TextView) v.findViewById(R.id.tv_userId);
        menu1 = (LinearLayout) v.findViewById(R.id.menu_1);
        menu2 = (LinearLayout) v.findViewById(R.id.menu_2);
        menu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(ViewTimeSlot.newInstance(getActivity()),"viewTimeSlot");
            }
        });
        menu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(AddTimeSlot.newInstance(getActivity(),null),"addTimeSlot");
            }
        });

        recycler = (RecyclerView)v.findViewById(R.id.main_recycler);
        sList=new ArrayList<>();
        adapter = new DashboardAdapter(getActivity(),Dashboard.this,sList);
        adapter.setMode(ExpandableRecyclerAdapter.MODE_ACCORDION);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler.setAdapter(adapter);
    }


    public void bookingSlotDetail(DashboardAdapter.BodyListItem sample) {
        PopupAlert.newInstance("Description", sample.Description, PopupAlert.POPUP_DIALOG)
                .show(getFragmentManager(), "popupAlert");
    }


    public void cancelSlot(DashboardAdapter.BodyListItem sample) {
        String consultantId=AppUser.getConsultantId(getActivity());
        executeTask(TASK_CANCEL, consultantId, sample.DateSlt,sample.TimeSlt);
    }

    public void updateSlotStatus(DashboardAdapter.BodyListItem sample) {
        UpdateSlotStatus.newInstance("Call Status", sample, Dashboard.this)
                .show(getFragmentManager(), "slotFeedback");
    }
    @Override
    public void onUpdateSlotOkClick(DialogFragment dialog, DashboardAdapter.BodyListItem sample, String type, String remarks) {
        String consultantId=AppUser.getConsultantId(getActivity());
        executeTask(TASK_STATUS_UPDATE, consultantId, sample.DateSlt, sample.TimeSlt,type, remarks);
    }


    public void sendFeedback(DashboardAdapter.BodyListItem sample) {
        SlotFeedback.newInstance("Feedback", sample, Dashboard.this)
                .show(getFragmentManager(), "slotFeedback");
    }
    @Override
    public void onSlotFeedbackOkClick(DialogFragment dialog, DashboardAdapter.BodyListItem sample, String feedback) {
        String consultantId=AppUser.getConsultantId(getActivity());
        executeTask(TASK_FEEDBACK, consultantId, sample.DateSlt, sample.TimeSlt,null, feedback);
    }



    private void executeTask(int task) {
        executeTask(task, null, null, null);
    }

    private void executeTask(int task, String consultantId, String date, String time) {
        executeTask(task, consultantId, date, time,null,null);
    }

    private void executeTask(int task, String consultantId, String date, String time,String statusId, String remarks) {
        assignTask = new AssignTask(getActivity(), task, consultantId, date,time,statusId, remarks);
        switch (task) {
            case TASK_LOADLIST:
                assignTask.execute(AppSettings.VIEW_BOOKED_SLOTS);
                break;
            case TASK_STATUS_UPDATE:
                // consultantId,statusId,date,time
                assignTask.execute(AppSettings.PUT_CONSULTANT_STATUS);
                break;
            case TASK_CANCEL:
                // consultantId,date,time
                assignTask.execute(AppSettings.CANCEL_SLOT);
                break;
            case TASK_FEEDBACK:
                // consultantId,date,time
                assignTask.execute(AppSettings.PUT_CONSULTANT_FEEDBACK);
                break;
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, ApiErrorModel apiModel) {
        executeTask(apiModel.getTask(), apiModel.getValue1(), apiModel.getValue2(),
                apiModel.getValue3(),apiModel.getValue4(),apiModel.getValue5());
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    public void callAction(final String phoneNo) {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNo));
                try {
                    getActivity().startActivity(in);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Could not find an activity to place the call.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getActivity(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }


        };

        new TedPermission(getActivity())
                .setPermissionListener(permissionlistener)
                .setRationaleMessage("Permission required for using this service")
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CALL_PHONE,Manifest.permission.READ_CALL_LOG)
                .setGotoSettingButtonText("setting")
                .check();
    }


    private class AssignTask extends AsyncTask<String, Void, BookingSlot> {
        private String consultantId, date,time,statusId,remarks;
        private Context context;
        private int task;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, int task, String consultantId, String date, String time
                          ,String statusId,String remarks) {
            this.context = context;
            this.consultantId = consultantId;
            this.date = date;
            this.time = time;
            this.statusId = statusId;
            this.remarks = remarks;
            this.task = task;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context, "Processing please wait.");
        }

        @Override
        protected BookingSlot doInBackground(String... urls) {
            String response;
            JsonParser jsonParser = new JsonParser();
            String consultantId = AppUser.getConsultantId(context);
            switch (task) {
                case TASK_LOADLIST:
                    response = ApiCall.POST(urls[0], RequestBuilder.Default(consultantId));
                    return jsonParser.parseDashboardData2(response);
                case TASK_STATUS_UPDATE:
                    response = ApiCall.POST(urls[0], RequestBuilder.UpdateStatus(consultantId, date, time,statusId, remarks));
                    return jsonParser.bookingSlotDefault(response);
                case TASK_CANCEL:
                    response = ApiCall.POST(urls[0], RequestBuilder.SlotCancel(consultantId, date, time));
                    return jsonParser.bookingSlotDefault(response);
                case TASK_FEEDBACK:
                    response = ApiCall.POST(urls[0], RequestBuilder.ConsultantFeedback(consultantId, date, time, remarks));
                    return jsonParser.bookingSlotDefault(response);
            }
            return null;
        }


        @Override
        protected void onPostExecute(BookingSlot result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        switch (task) {
                            case TASK_LOADLIST:
                                sList.addAll(result.getList());
                                adapter.setListItems(sList);
                                adapter.notifyDataSetChanged();
//                                adapter = new BookingSlotAdapter(getActivity(), R.layout.slot_bookings, , Dashboard.this);
//                                lvMain.setAdapter(adapter);
                                break;
                            case TASK_STATUS_UPDATE:
                                PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                        .show(getFragmentManager(), "popupAlert");
                                break;
                            case TASK_CANCEL:
                                PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                        .show(getFragmentManager(), "popupAlert");
                                break;
                            case TASK_FEEDBACK:
                                PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                        .show(getFragmentManager(), "popupAlert");
                                break;
                        }
                    } else if (result.getOutput().equals("failure")) {
                        PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    }
                } else {
                    ApiErrorModel model = new ApiErrorModel();
                    model.setTask(task);
                    model.setFields(consultantId,date,time,statusId,remarks);
                    ApiCallError.newInstance(JsonParser.ERRORMESSAGE, model, Dashboard.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }
        }
    }

}
