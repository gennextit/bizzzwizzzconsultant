package com.gennext.bizzzwizzzconsultant.app;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.common.PopupAlert;
import com.gennext.bizzzwizzzconsultant.common.PopupDialog;
import com.gennext.bizzzwizzzconsultant.common.TimeSlotEditDialog;
import com.gennext.bizzzwizzzconsultant.model.ApiErrorModel;
import com.gennext.bizzzwizzzconsultant.model.DialogModel;
import com.gennext.bizzzwizzzconsultant.model.TimeSlotAdapter;
import com.gennext.bizzzwizzzconsultant.model.TimeSlotModel;
import com.gennext.bizzzwizzzconsultant.util.ApiCall;
import com.gennext.bizzzwizzzconsultant.util.ApiCallError;
import com.gennext.bizzzwizzzconsultant.util.AppAnimation;
import com.gennext.bizzzwizzzconsultant.util.AppSettings;
import com.gennext.bizzzwizzzconsultant.util.AppUser;
import com.gennext.bizzzwizzzconsultant.util.CompactFragment;
import com.gennext.bizzzwizzzconsultant.util.JsonParser;
import com.gennext.bizzzwizzzconsultant.util.RequestBuilder;


/**
 * Created by Abhijit-PC on 18-Feb-17.
 */

public class ViewTimeSlot extends CompactFragment implements TimeSlotEditDialog.TimeslotEditListener, PopupDialog.DialogDataListener, ApiCallError.ErrorFlagListener {

    private ListView lvMain;
    private AssignTask assignTask;
    public static final int TASK_LOADLIST = 1, TASK_EDIT = 2, TASK_DELETE = 3;
    private TimeSlotAdapter adapter;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static ViewTimeSlot newInstance(Activity act) {
        ViewTimeSlot fragment = new ViewTimeSlot();
        AppAnimation.setDialogAnimation(act, fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_timeslot_view, container, false);
        initToolBar(getActivity(), v, "Time Slot");
        InitUI(v);
        executeTask(TASK_LOADLIST);
        return v;
    }

    private void InitUI(View v) {
        lvMain = (ListView) v.findViewById(R.id.lv_main);
        final ImageView toolbarMenu = (ImageView) v.findViewById(R.id.iv_menu);
        toolbarMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(toolbarMenu);
            }
        });
    }

    private void showPopup(ImageView imageView) {
        final PopupMenu popup = new PopupMenu(getActivity(), imageView);
        popup.getMenuInflater().inflate(R.menu.add_time_slot, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int i = item.getItemId();
                if (i == R.id.menu_add_time_slot_option) {
                    addFragment(AddTimeSlot.newInstance(getActivity(), ViewTimeSlot.this), "addTimeSlot");
                    return true;
                } else {
                    return onMenuItemClick(item);
                }
            }
        });
        popup.show();
    }


    public void executeTask(int task) {
        executeTask(task, null, null, null, null, null);
    }

    private void executeTask(int task, String slotId, String date, String sTime, String eTime, String days) {
        if (task == TASK_LOADLIST) {
            assignTask = new AssignTask(getActivity(), task, slotId, date, sTime, eTime, days);
            assignTask.execute(AppSettings.LOAD_TIMESLOT);
        } else if (task == TASK_DELETE) {//consultantId,date,startTime,endTime
            assignTask = new AssignTask(getActivity(), task, slotId, date, sTime, eTime, days);
            assignTask.execute(AppSettings.DELETE_TIMESLOT);
        } else if (task == TASK_EDIT) {//startTime,endTime,id
            assignTask = new AssignTask(getActivity(), task, slotId, date, sTime, eTime, days);
            assignTask.execute(AppSettings.EDIT_TIMESLOT);
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, ApiErrorModel apiModel) {
        if (apiModel.getTask() == TASK_LOADLIST) {
            executeTask(apiModel.getTask());
        } else if (apiModel.getTask() == TASK_DELETE) {
            executeTask(apiModel.getTask(), apiModel.getValue1(), apiModel.getValue2(), apiModel.getValue3()
                    , apiModel.getValue4(), apiModel.getValue5());
        } else if (apiModel.getTask() == TASK_EDIT) {
            executeTask(apiModel.getTask(), apiModel.getValue1(), apiModel.getValue2(), apiModel.getValue3()
                    , apiModel.getValue4(), apiModel.getValue5());
        }
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    public void editTimeSlot(TimeSlotModel model) {
        TimeSlotEditDialog.newInstance(model, ViewTimeSlot.this)
                .show(getFragmentManager(), "timeSlotEditDialog");
    }

    public void deleteTimeSlot(TimeSlotModel model) {
        DialogModel dialogModel = new DialogModel();
        dialogModel.setValue1(model.getSlotId());
        dialogModel.setValue2(model.getDate());
        dialogModel.setValue3(model.getStartTime());
        dialogModel.setValue4(model.getEndTime());
        PopupDialog.newInstance("Alert", "Are you sure to delete this time slot", dialogModel, ViewTimeSlot.this)
                .show(getFragmentManager(), "popupDialog");
    }

    @Override
    public void onOkClick(DialogFragment dialog, DialogModel model) {
        executeTask(TASK_DELETE, model.getValue1(), model.getValue2(), model.getValue3(), model.getValue4(), null);
    }

    @Override
    public void onCancelClick(DialogFragment dialog, DialogModel dialogModel) {

    }

    @Override
    public void onTimeSlotEditClick(DialogFragment dialog, TimeSlotModel model) {
        executeTask(TASK_EDIT, model.getSlotId(), model.getDate(), model.getStartTime(), model.getEndTime(), model.getDay());
    }


    private class AssignTask extends AsyncTask<String, Void, TimeSlotModel> {

        private String slotId, date, sTime, eTime, daya;
        private int task;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, int task, String slotId, String date, String sTime, String eTime, String daya) {
            this.slotId = slotId;
            this.date = date;
            this.sTime = sTime;
            this.eTime = eTime;
            this.daya = daya;
            this.context = context;
            this.task = task;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (task == TASK_LOADLIST) {
                showProgressDialog(context, "Loading please wait.");
            } else if (task == TASK_DELETE) {
                showProgressDialog(context, "Processing please wait.");
            } else if (task == TASK_EDIT) {
                showProgressDialog(context, "Updating data please wait.");
            }
        }

        @Override
        protected TimeSlotModel doInBackground(String... urls) {
            String response;
            if (task == TASK_LOADLIST) {
                String consultantId = AppUser.getConsultantId(context);
                response = ApiCall.POST(urls[0], RequestBuilder.Default(consultantId));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseTimeSlotsData(response);
            } else if (task == TASK_DELETE) {
                String consultantId = AppUser.getConsultantId(context);
                response = ApiCall.POST(urls[0], RequestBuilder.DeleteTimeSlot(consultantId, slotId, date, sTime, eTime));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseEditOrDeleteTimeSlot(response);
            } else {
                String consultantId = AppUser.getConsultantId(context);
                response = ApiCall.POST(urls[0], RequestBuilder.EditTimeSlot(consultantId, slotId, date, sTime, eTime, daya));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseEditOrDeleteTimeSlot(response);
            }
        }

        @Override
        protected void onPostExecute(TimeSlotModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (task == TASK_LOADLIST) {
                            if (adapter == null) {
                                adapter = new TimeSlotAdapter(getActivity(), R.layout.slot_bookings,result.getList(), ViewTimeSlot.this);
                                lvMain.setAdapter(adapter);
                            }else{
                                adapter.clear();
                                adapter.addAll(result.getList());
                                adapter.notifyDataSetChanged();
                            }

                        } else if (task == TASK_EDIT) {
                            executeTask(TASK_LOADLIST);
                        } else if (task == TASK_DELETE) {
                            executeTask(TASK_LOADLIST);
                        }
                    } else if (result.getOutput().equals("failure")) {
                        PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    }
                } else {
                    ApiErrorModel apiErrorModel = new ApiErrorModel();
                    apiErrorModel.setTask(task);
                    apiErrorModel.setValue1(slotId);
                    apiErrorModel.setValue2(date);
                    apiErrorModel.setValue3(sTime);
                    apiErrorModel.setValue4(eTime);
                    apiErrorModel.setValue5(daya);
                    ApiCallError.newInstance(JsonParser.ERRORMESSAGE, apiErrorModel, ViewTimeSlot.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }
        }

    }
} 
