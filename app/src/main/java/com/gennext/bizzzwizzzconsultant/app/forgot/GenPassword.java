package com.gennext.bizzzwizzzconsultant.app.forgot;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gennext.bizzzwizzzconsultant.ForgotPassActivity;
import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.common.PopupAlert;
import com.gennext.bizzzwizzzconsultant.model.Model;
import com.gennext.bizzzwizzzconsultant.util.ApiCall;
import com.gennext.bizzzwizzzconsultant.util.ApiCallError;
import com.gennext.bizzzwizzzconsultant.util.AppAnimation;
import com.gennext.bizzzwizzzconsultant.util.AppSettings;
import com.gennext.bizzzwizzzconsultant.util.CompactFragment;
import com.gennext.bizzzwizzzconsultant.util.JsonParser;
import com.gennext.bizzzwizzzconsultant.util.RequestBuilder;


/**
 * Created by Abhijit on 14-Oct-16.
 */

public class GenPassword extends CompactFragment implements ApiCallError.ErrorListener{
    private Button btnOK;
    AssignTask assignTask;
    private EditText etPassword,etPasswordConferm;
    private int verifyType;
    private String input;
    private String title;

    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static GenPassword newInstance(Activity act, String title,int verifyType,String input) {
        GenPassword fragment=new GenPassword();
        fragment.title=title;
        fragment.verifyType=verifyType;
        fragment.input=input;
        AppAnimation.setDialogAnimation(act,fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_forgot_pass_gen, container, false);
        initToolBarForActivity(getActivity(),v,title);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        btnOK = (Button) v.findViewById(R.id.btn_submit);
        etPassword = (EditText) v.findViewById(R.id.et_verify_pass);
        etPasswordConferm = (EditText) v.findViewById(R.id.et_verify_pass_confirm);




        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                executeTask();
            }
        });
    }

    private void executeTask() {
        String pass=etPassword.getText().toString();
        if(!pass.equals("")&&pass.equalsIgnoreCase(etPasswordConferm.getText().toString())) {
            if(verifyType== ForgotPassActivity.MOBILE) {
                assignTask = new AssignTask(getActivity(), pass, input);
                assignTask.execute(AppSettings.CREATE_PASS_MOBILE);
            }else if(verifyType==ForgotPassActivity.EMAIL){
                assignTask = new AssignTask(getActivity(), pass, input);
                assignTask.execute(AppSettings.CREATE_PASS_EMAIL);
            }
        }else{
            Toast.makeText(getActivity(), "Password did not match.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }



    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String password,input;
        private Context activity;

        public void onAttach(Context activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, String password, String input) {
            this.password = password;
            this.input = input;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Processing please wait...");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response="";
            if(verifyType==ForgotPassActivity.MOBILE){
                response = ApiCall.POST(urls[0], RequestBuilder.createPassThroughMobile(password, input));
            }else if(verifyType==ForgotPassActivity.EMAIL){
                response = ApiCall.POST(urls[0], RequestBuilder.createPassThroughEmail(password, input));
            }
            JsonParser jsonParser = new JsonParser();
            return jsonParser.defaultParser(response);
        }

        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        Toast.makeText(activity, "Successful created new passoword, Please login with your new password", Toast.LENGTH_LONG).show();
//                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        getActivity().finish();
                    } else if (result.getOutput().equals("failure")) {
                        PopupAlert.newInstance("Feedback",result.getOutputMsg(),PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(),"popupAlert");
                    } else {
                        PopupAlert.newInstance("Alert",result.getOutputMsg(),PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(),"popupAlert");
                    }
                } else {
                    ApiCallError.newInstance(JsonParser.ERRORMESSAGE,GenPassword.this)
                            .show(getFragmentManager(),"apiCallError");

                }
            }
        }
    }
}