package com.gennext.bizzzwizzzconsultant.util;

/**
 * Created by Abhijit on 13-Dec-16.
 */

/**
 * ERROR-101 (Unformetted Json Response)
 * ERROR-102 ()
 */
public class AppSettings {

    public static final String SERVER = "http://bizzzwizzz.com";
    public static final String COMMON = SERVER+ "/index.php/ConsultantRest/";

    public static final String SEND_FEEDBACK  = COMMON + "sendFeedback "; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook
    public static final String TERMS_OF_USE  = SERVER + "/files/Terms%20of%20Use.pdf"; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook
    public static final String PRIVACY_POLICY  = SERVER + "/files/Privacy%20Policy.pdf"; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook

    public static final String VERIFY_CONSULTANT = COMMON + "verifyConsultant";//userId,password
    public static final String VIEW_BOOKED_SLOTS = COMMON + "viewBookedSlots";//userId
    public static final String CANCEL_SLOT = COMMON + "cancelSlot";// consultantId,date,time
    public static final String PUT_CONSULTANT_STATUS = COMMON + "putConsultantStatus";// consultantId,statusId,date,time
    public static final String PUT_CONSULTANT_FEEDBACK =  COMMON + "putConsultantFeedback";//consultantId,feedback,date,time
    public static final String LOAD_TIMESLOT = COMMON + "viewSlots";//consultantId

    public static final String DELETE_TIMESLOT =  COMMON + "deleteTimeSlot";//consultantId,date,startTime,endTime
    public static final String EDIT_TIMESLOT =  COMMON + "editTimeSlot";//startTime,endTime,id
    public static final String ADD_TIME_SLOT =  COMMON + "addTimeSlot";//consultantId,startDate,endDate,startTime,endTime,days

    public static final String UDPATE_SERVER_RECORD = COMMON + "updateCallRecordToServer";//consultantId,callJson;


    public static final String LOAD_DOMAIN = COMMON + "getDomainList";//removed
    public static final String UPDATE_DOMAIN = COMMON + "updateDomain";//removed

    // required API's for Consultant
    public static final String VERIFY_MOBILE = COMMON + "verifyMobile";//moblie
    public static final String VERIFY_MOBILE_OTP = COMMON + "verifyOTPMobile";//moblie,otp

    public static final String VERIFY_EMAIL = COMMON + "verifyEmail";//email;
    public static final String VERIFY_EMAIL_OTP = COMMON + "verifyOTPEmail";//email,otp;

    public static final String CREATE_PASS_MOBILE = COMMON + "createPassThroughMobile";//mobile,password;
    public static final String CREATE_PASS_EMAIL = COMMON + "createPassThroughEmail";//email,password;
}

/* New API's required for BizzzWizzz Consultant App


*/

/* New API's required for BizzzWizzz App

API 1:   http://bizzzwizzz.com/index.php/Rest/signup

Params: name,mobile,email,panNo,pass,profileType

Response: success/failure.(return success and basic info similar to login api)

*/

/*
    1 1xx Informational.
    2 2xx Success.
    3 3xx Redirection.
    4 4xx Client Error.
    5 5xx Server Error.
*/
