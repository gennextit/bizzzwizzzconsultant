package com.gennext.bizzzwizzzconsultant.util;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;

import com.gennext.bizzzwizzzconsultant.model.CallModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CallLogHelper {

    public CallModel getCallLogs(ContentResolver cr,String phoneNo) {
        CallModel callModel = new CallModel();
        callModel.setList(new ArrayList<CallModel>());
        Cursor curLog=getAllCallLogs(cr);
        while (curLog.moveToNext()) {
            String callNumber = curLog.getString(curLog
                    .getColumnIndex(android.provider.CallLog.Calls.NUMBER));
            if(callNumber.equalsIgnoreCase(phoneNo)){
                CallModel slot = new CallModel();
                slot.setPhoneNo(callNumber);

                String callName = curLog
                        .getString(curLog
                                .getColumnIndex(android.provider.CallLog.Calls.CACHED_NAME));
                if (callName == null) {
                    slot.setName("Unknown");
                } else
                    slot.setName(callName);

                String callDate = curLog.getString(curLog
                        .getColumnIndex(android.provider.CallLog.Calls.DATE));
                SimpleDateFormat formatter = new SimpleDateFormat(
                        "yyyy-MM-dd HH:mm");
                String dateString = formatter.format(new Date(Long
                        .parseLong(callDate)));
                slot.setCallDayTime(dateString);

                String callType = curLog.getString(curLog
                        .getColumnIndex(android.provider.CallLog.Calls.TYPE));
                if (callType.equals("1")) {
                    slot.setCallType("Incoming");
                } else
                    slot.setCallType("Outgoing");

                String duration = curLog.getString(curLog
                        .getColumnIndex(android.provider.CallLog.Calls.DURATION));
                slot.setCallDuration(duration);
                callModel.getList().add(slot);
            }
        }
        return callModel;
    }

    public String getCallLogs2(ContentResolver cr,String phoneNo) {
        StringBuilder sb=new StringBuilder();
        String caller,clType;
        Cursor curLog=getAllCallLogs(cr);
        while (curLog.moveToNext()) {
            String callNumber = curLog.getString(curLog
                    .getColumnIndex(android.provider.CallLog.Calls.NUMBER));
            if(callNumber.equalsIgnoreCase(phoneNo)){

                String callName = curLog
                        .getString(curLog
                                .getColumnIndex(android.provider.CallLog.Calls.CACHED_NAME));
                if (callName == null) {
                    caller="Unknown";
                } else
                    caller=callName;

                String callDate = curLog.getString(curLog
                        .getColumnIndex(android.provider.CallLog.Calls.DATE));
                SimpleDateFormat formatter = new SimpleDateFormat(
                        "yyyy-MM-dd HH:mm");
                String dateString = formatter.format(new Date(Long
                        .parseLong(callDate)));

                String callType = curLog.getString(curLog
                        .getColumnIndex(android.provider.CallLog.Calls.TYPE));
                if (callType.equals("1")) {
                    clType="Incoming";
                } else
                    clType="Outgoing";

                String duration = curLog.getString(curLog
                        .getColumnIndex(android.provider.CallLog.Calls.DURATION));

                sb.append("\nPhone Number:--- " + callNumber + " \nCall Type:--- "
                        + clType + " \nCall Date:--- " + dateString
                        + " \nCall duration in sec :--- " + duration);
                sb.append("\n----------------------------------");
            }
        }
        return sb.toString();
    }

    public static Cursor getAllCallLogs(ContentResolver cr) {
        // reading all data in descending order according to DATE
        String strOrder = CallLog.Calls.DATE + " DESC";
        Uri callUri = Uri.parse("content://call_log/calls");
        Cursor curCallLogs = cr.query(callUri, null, null, null, strOrder);

        return curCallLogs;
    }

//	public static void insertPlaceholderCall(ContentResolver contentResolver,
//			String name, String number) {
//		ContentValues values = new ContentValues();
//		values.put(CallLog.Calls.NUMBER, number);
//		values.put(CallLog.Calls.DATE, System.currentTimeMillis());
//		values.put(CallLog.Calls.DURATION, 0);
//		values.put(CallLog.Calls.TYPE, CallLog.Calls.OUTGOING_TYPE);
//		values.put(CallLog.Calls.NEW, 1);
//		values.put(CallLog.Calls.CACHED_NAME, name);
//		values.put(CallLog.Calls.CACHED_NUMBER_TYPE, 0);
//		values.put(CallLog.Calls.CACHED_NUMBER_LABEL, "");
//		Log.d("Call Log", "Inserting call log placeholder for " + number);
//		contentResolver.insert(CallLog.Calls.CONTENT_URI, values);
//	}

}
