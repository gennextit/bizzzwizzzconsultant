package com.gennext.bizzzwizzzconsultant.util;

import android.content.Context;

import com.gennext.bizzzwizzzconsultant.model.BookingSlot;
import com.gennext.bizzzwizzzconsultant.model.CallModel;
import com.gennext.bizzzwizzzconsultant.model.DomainsModel;
import com.gennext.bizzzwizzzconsultant.model.Model;
import com.gennext.bizzzwizzzconsultant.model.StatusModel;
import com.gennext.bizzzwizzzconsultant.model.TimeSlotModel;
import com.gennext.bizzzwizzzconsultant.model.WeekDaysModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */
public class JsonParser {
    public static String ERRORMESSAGE = "Not Available";


    public Model defaultParser(String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString()+"\n"+response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public Model parseLoginData(Context activity, String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    JSONObject msgObj=mainObject.optJSONObject("message");
                    if(msgObj!=null){
                        AppUser.setLoginData(activity,msgObj);
                    }
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString()+"\n"+response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public BookingSlot parseDashboardData2(String response) {
        String dateSaprator = "", tempDate = "",status="";
        BookingSlot model = new BookingSlot();
        model.setOutput("na");
        model.setOutputMsg("NA");
        model.setList(new ArrayList<BookingSlot>());
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    JSONObject jsonObject=mainObject.optJSONObject("message");
                    if(jsonObject!=null){
                        String currentDate=jsonObject.optString("currentDate");
                        JSONArray jsonArray=jsonObject.optJSONArray("details");
                        if(jsonArray!=null)
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject msgObj=jsonArray.optJSONObject(i);
                            model.setOutput("success");
                            tempDate = msgObj.optString("Date");// "2017-02-20";
                            if (tempDate != dateSaprator) {
                                if(currentDate.equals(tempDate)){
                                    status="progress";
                                }else{
                                    status="inprocess";
                                    status="progress";
                                }
                                dateSaprator = tempDate;
                                model.getList().add(setDataSaprator(tempDate, BookingSlot.TYPE_SEPARATOR));
                            }
                            model.getList().add(setSlotData(msgObj.optString("UserId"),msgObj.optString("slotId"), msgObj.optString("name")
                                    , tempDate,  msgObj.optString("Time"), msgObj.optString("mobile"),msgObj.optString("Description")
                                    , status, BookingSlot.TYPE_ITEM));

                        }
                    }else{
                        model.setOutput("failure");
                        model.setOutputMsg("Empty booked slot list");
                    }
                } else if (mainObject.getString("status").equals("failure")) {
                    model.setOutput("failure");
                    model.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString()+"\n"+response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }

        return model;
    }



    private BookingSlot setSlotData(String userId,String slotId, String name, String date, String time, String mobile
            , String description, String status, int itemType) {
        BookingSlot bookingSlot = new BookingSlot();
        bookingSlot.setFields(userId,slotId, name, date, time,mobile,description,status, itemType);
        return bookingSlot;
    }

    private BookingSlot setDataSaprator(String date, int itemType) {
        BookingSlot bookingSlot = new BookingSlot();
        bookingSlot.setDateSaprator(date, itemType);
        return bookingSlot;
    }

    public TimeSlotModel parseTimeSlotsData(String response) {
        TimeSlotModel model = new TimeSlotModel();
        model.setOutput("success");
        model.setOutputMsg("NA");
        model.setList(new ArrayList<TimeSlotModel>());
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    model.setOutput("success");
                    JSONArray msgArr=mainObject.optJSONArray("message");
                    if(msgArr!=null){
                        for (int i=0;i<msgArr.length();i++){
                            JSONObject msgObj=msgArr.optJSONObject(i);
                            model.getList().add(setTimeSlotData(msgObj.optString("id"),msgObj.optString("Date"),
                                    msgObj.optString("Days"),msgObj.optString("StartTime"),msgObj.optString("EndTime")
                                    ,msgObj.optString("Available")));
                        }
                    }
                } else if (mainObject.getString("status").equals("failure")) {
                    model.setOutput("failure");
                    model.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString()+"\n"+response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return model;
    }

    public TimeSlotModel parseTimeSlotsData() {
        TimeSlotModel model = new TimeSlotModel();
        model.setOutput("success");
        model.setOutputMsg("NA");
        model.setList(new ArrayList<TimeSlotModel>());

//        model.getList().add(setTimeSlotData("01","10-02-2017", "Friday","10:30","18:30"));
//        model.getList().add(setTimeSlotData("02","11-02-2017", "Saturday","12:30","18:00"));
//        model.getList().add(setTimeSlotData("03","12-02-2017", "Sunday","10:30","18:30"));
//        model.getList().add(setTimeSlotData("04","13-02-2017", "Monday","11:30","15:30"));
//        model.getList().add(setTimeSlotData("05","14-02-2017", "Tuesday","10:30","15:30"));

        return model;
    }

    private TimeSlotModel setTimeSlotData(String slotId,String date, String day, String sTime, String eTime,String Available) {
        TimeSlotModel model=new TimeSlotModel();
        model.setFields(slotId,date,day,sTime,eTime,Available);
        return model;
    }

    public TimeSlotModel parseEditOrDeleteTimeSlot(String response) {
        TimeSlotModel jsonModel = new TimeSlotModel();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString()+"\n"+response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public static WeekDaysModel parseDayAndTimingIdJson() {
        WeekDaysModel model = new WeekDaysModel();
        model.setList(new ArrayList<WeekDaysModel>());

        model.getList().add(setSlotWeekDaysModel("1","Monday"));
        model.getList().add(setSlotWeekDaysModel("2","Tuesday"));
        model.getList().add(setSlotWeekDaysModel("3","Wednesday"));
        model.getList().add(setSlotWeekDaysModel("4","Thursday"));
        model.getList().add(setSlotWeekDaysModel("5","Friday"));
        model.getList().add(setSlotWeekDaysModel("6","Saturday"));
        model.getList().add(setSlotWeekDaysModel("7","Sunday"));

        return model;
    }

    private static WeekDaysModel setSlotWeekDaysModel(String id,String day) {
        WeekDaysModel slot = new WeekDaysModel();
        slot.setFields(id,day,false);
        return slot;
    }
    public BookingSlot bookingSlotDefault(String response) {
        BookingSlot jsonModel = new BookingSlot();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString()+"\n"+response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public StatusModel parseStatusModel(String response) {
        StatusModel jsonModel = new StatusModel();
        jsonModel.setList(new ArrayList<StatusModel>());
        if (response.contains("[")) {
            try {
                StatusModel sp = new StatusModel();
                sp.setId(null);
                sp.setName("Select status type");
                jsonModel.getList().add(sp);
                JSONArray mainArray = new JSONArray(response);
                for(int i=0;i<mainArray.length();i++){
                    JSONObject mainObject = mainArray.optJSONObject(i);
                    StatusModel model=new StatusModel();
                    model.setId(mainObject.optString("id"));
                    model.setName(mainObject.optString("Name"));
                    jsonModel.getList().add(model);
                }

            } catch (JSONException e) {
                ERRORMESSAGE = e.toString()+"\n"+response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public static String toCalHistoryJson(ArrayList<CallModel> list) {
        try {
            // In this case we need a json array to hold the java list
            JSONArray jsonArr = new JSONArray();
            if (list != null) {
                for (CallModel model : list) {
                    JSONObject pnObj = new JSONObject();
                    pnObj.put("phoneNo", model.getPhoneNo());
                    pnObj.put("callType", model.getCallType());
                    pnObj.put("callDayTime", model.getCallDayTime());
                    pnObj.put("callDuration", model.getCallDuration());
                    jsonArr.put(pnObj);
                }
            } else {
                return null;
            }
            return jsonArr.toString();


        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public DomainsModel parseDomainData() {
        DomainsModel model = new DomainsModel();
        model.setOutput("success");
        model.setOutputMsg("NA");
        model.setList(new ArrayList<DomainsModel>());

        model.getList().add(setDomainsSlotData("dom1", "Residential status and scope of total income"));
        model.getList().add(setDomainsSlotData("dom2", "Income from Salary including Pension"));
        model.getList().add(setDomainsSlotData("dom3", "Income from House Property (rental Income"));
        model.getList().add(setDomainsSlotData("dom4", "Income from Business or Profession"));
        model.getList().add(setDomainsSlotData("dom5", "Income from Capital Gain (Sale of property or shares etc.)"));
        model.getList().add(setDomainsSlotData("dom6", "Income from Other Sources"));
        model.getList().add(setDomainsSlotData("dom7", "Agriculture Income"));
        model.getList().add(setDomainsSlotData("dom8", "Filing of Return"));
        model.getList().add(setDomainsSlotData("dom9", "Set-off or carry forward and set-off of  losses"));
        model.getList().add(setDomainsSlotData("dom10", "Deductions, Exemptions, Rebate from Income"));

        return model;
    }

    private DomainsModel setDomainsSlotData(String id, String name) {
        DomainsModel domainsModel = new DomainsModel();
        domainsModel.setFields(id, name);
        return domainsModel;
    }

    public DomainsModel parseUpdateDomain(String response) {
        DomainsModel jsonModel = new DomainsModel();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString()+"\n"+response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    //    public BookingSlot parseDashboardData(String response) {
//        String dateSaprator = "", tempDate = "";
//        String desc="You know that MLM success works but what did they do specifically that others don’t?";
//        BookingSlot model = new BookingSlot();
//        model.setOutput("success");
//        model.setOutputMsg("NA");
//        model.setList(new ArrayList<BookingSlot>());
//        tempDate = "2017-02-20";
//        if (tempDate != dateSaprator) {
//            dateSaprator = tempDate;
//            model.getList().add(setDataSaprator(tempDate, BookingSlot.TYPE_SEPARATOR));
//        }
//        model.getList().add(setSlotData("BW10000001","01", "Abhijit", tempDate, "13:10","9876543211",desc,"progress", BookingSlot.TYPE_ITEM));
//
//        tempDate = "2017-02-20";
//        if (tempDate != dateSaprator) {
//            dateSaprator = tempDate;
//            model.getList().add(setDataSaprator(tempDate, BookingSlot.TYPE_SEPARATOR));
//        }
//        model.getList().add(setSlotData("BW10000002","02", "Pankaj", tempDate, "15:30","9876543211",desc,"progress", BookingSlot.TYPE_ITEM));
//        tempDate = "2017-02-21";
//        if (tempDate != dateSaprator) {
//            dateSaprator = tempDate;
//            model.getList().add(setDataSaprator(tempDate, BookingSlot.TYPE_SEPARATOR));
//        }
//        model.getList().add(setSlotData("BW10000003","03", "Sunny", tempDate, "17:20","9876543211",desc,"inprocess", BookingSlot.TYPE_ITEM));
//        tempDate = "2017-02-21";
//        if (tempDate != dateSaprator) {
//            dateSaprator = tempDate;
//            model.getList().add(setDataSaprator(tempDate, BookingSlot.TYPE_SEPARATOR));
//        }
//        model.getList().add(setSlotData("BW10000004","04", "Arpit", tempDate, "18:50","9876543211",desc,"inprocess", BookingSlot.TYPE_ITEM));
//
//        tempDate = "2017-02-21";
//        if (tempDate != dateSaprator) {
//            dateSaprator = tempDate;
//            model.getList().add(setDataSaprator(tempDate, BookingSlot.TYPE_SEPARATOR));
//        }
//        model.getList().add(setSlotData("BW10000005","05", "Poonam", tempDate, "13:10","9876543211",desc,"inprocess", BookingSlot.TYPE_ITEM));
//        tempDate = "2017-02-21";
//        if (tempDate != dateSaprator) {
//            dateSaprator = tempDate;
//            model.getList().add(setDataSaprator(tempDate, BookingSlot.TYPE_SEPARATOR));
//        }
//        model.getList().add(setSlotData("BW10000006","06", "Dwarkesh", tempDate, "15:30","9876543211",desc,"cancel", BookingSlot.TYPE_ITEM));
//        tempDate = "2017-02-22";
//        if (tempDate != dateSaprator) {
//            dateSaprator = tempDate;
//            model.getList().add(setDataSaprator(tempDate, BookingSlot.TYPE_SEPARATOR));
//        }
//        model.getList().add(setSlotData("BW10000007","07", "Akash", tempDate, "17:20","9876543211",desc,"pending", BookingSlot.TYPE_ITEM));
//
//        tempDate = "2017-02-22";
//        if (tempDate != dateSaprator) {
//            dateSaprator = tempDate;
//            model.getList().add(setDataSaprator(tempDate, BookingSlot.TYPE_SEPARATOR));
//        }
//        model.getList().add(setSlotData("BW10000008","08", "Jimmy", tempDate, "18:50","9876543211",desc,"inprocess", BookingSlot.TYPE_ITEM));
//        tempDate = "2017-02-17";
//        if (tempDate != dateSaprator) {
//            dateSaprator = tempDate;
//            model.getList().add(setDataSaprator(tempDate, BookingSlot.TYPE_SEPARATOR));
//        }
//        model.getList().add(setSlotData("BW10000009","09", "Joker", tempDate, "18:50","9876543211",desc,"inprocess", BookingSlot.TYPE_ITEM));
//        return model;
//    }

}
