package com.gennext.bizzzwizzzconsultant.util;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.bizzzwizzzconsultant.R;
import com.gennext.bizzzwizzzconsultant.model.ApiErrorModel;


public class ApiCallError extends DialogFragment {
    private String errorMessage;
    private TextView tvDescription;
    private Dialog dialog;
    private String mTitle;
    private String mMessage;
    private ErrorListener mListener;
    private ErrorFlagListener mFlagListener;
    private ApiErrorModel apiModel;

    public interface ErrorListener {
        void onErrorRetryClick(DialogFragment dialog);
        void onErrorCancelClick(DialogFragment dialog);
    }
    public interface ErrorFlagListener {
        void onErrorRetryClick(DialogFragment dialog, ApiErrorModel apiModel);
        void onErrorCancelClick(DialogFragment dialog);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static ApiCallError newInstance(String title, String message, String errorMessage, ErrorListener listener) {
        ApiCallError fragment = new ApiCallError();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.mListener = listener;
        fragment.errorMessage=errorMessage;
        return fragment;
    }
    public static ApiCallError newInstance(String errorMessage, ErrorListener listener) {
        ApiCallError fragment = new ApiCallError();
        fragment.mListener = listener;
        fragment.errorMessage=errorMessage;
        return fragment;
    }
    public static ApiCallError newInstance(String errorMessage, ApiErrorModel apiModel, ErrorFlagListener listener) {
        ApiCallError fragment = new ApiCallError();
        fragment.mFlagListener = listener;
        fragment.apiModel=apiModel;
        fragment.errorMessage=errorMessage;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        if(mTitle==null){
            mTitle=getString(R.string.server_time_out_tag);
            mMessage=getString(R.string.server_time_out_msg);
        }
        tvTitle.setText(mTitle);
        tvDescription.setText(mMessage);
        button1.setText("Retry");
        button2.setText("Cancel");
        ivAbout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (errorMessage != null) {
                    tvDescription.setText(errorMessage);
                }
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if(mListener != null) {
                    mListener.onErrorRetryClick(ApiCallError.this);
                }else if(mFlagListener != null) {
                    mFlagListener.onErrorRetryClick(ApiCallError.this,apiModel);
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if(mListener != null) {
                    mListener.onErrorCancelClick(ApiCallError.this);
                }else if(mFlagListener != null) {
                    mFlagListener.onErrorCancelClick(ApiCallError.this);
                }
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
