package com.gennext.bizzzwizzzconsultant.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;

import org.json.JSONObject;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class AppUser {
    public static final String COMMON = "bwConsultant";
    private static final String REMEMBER_USER = "remUser" + COMMON;
    private static final String REMEMBER_PASS = "remPass" + COMMON;
    private static final String APP_USER = "appuser" + COMMON;
    private static final String APP_PASS = "apppass" + COMMON;

    private static final String CONSULTANT_ID = "conId" + COMMON;

    private static final String STORED_DOMAINS = "sDomains" + COMMON;
    private static final String STORED_DAYS = "sDays" + COMMON;
    private static final String STORED_ALLSTATUS = "allStatus" + COMMON;
    private static final String ACTIVE_PHONE_NO = "activephoneno" + COMMON;


    public static void setLoginData(Context context, JSONObject msgObj) {
        if (context != null && msgObj != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (msgObj.optJSONArray("domains") != null) {
                editor.putString(STORED_DOMAINS, msgObj.optJSONArray("domains").toString());
            }else{
                editor.putString(STORED_DOMAINS, "");
            }
            if (msgObj.optJSONArray("days") != null) {
                editor.putString(STORED_DAYS, msgObj.optJSONArray("days").toString());
            }else{
                editor.putString(STORED_DAYS, "");
            }
            if (msgObj.optJSONArray("allStatus") != null) {
                editor.putString(STORED_ALLSTATUS, msgObj.optJSONArray("allStatus").toString());
            }else{
                editor.putString(STORED_ALLSTATUS, "");
            }
            if (msgObj.optString("consultantId") != null) {
                editor.putString(CONSULTANT_ID, msgObj.optString("consultantId"));
            }else{
                editor.putString(CONSULTANT_ID, "");
            }

            editor.apply();
        }
    }


    public static String LoadPref(Context context, String key) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String data = sharedPreferences.getString(key, "");
            return data;
        }
        return "";
    }

    public static String getName(Activity act) {
        return "";
    }

    public static String getImage(Activity act) {
        return "";
    }

    public static void setRememberUser(Context act, String username) {
        Utility.SavePref(act, REMEMBER_USER, username);
    }

    public static String getRememberUser(Context act) {
        return LoadPref(act, REMEMBER_USER);
    }

    public static void setRememberPass(Context act, String password) {
        Utility.SavePref(act, REMEMBER_PASS, password);
    }

    public static String getRememberPass(Context act) {
        return LoadPref(act, REMEMBER_PASS);
    }

    public static void setAppUserAndPass(Context act, String username, String password) {
        Utility.SavePref(act, APP_USER, username);
        Utility.SavePref(act, APP_PASS, password);
    }

    public static String getUserId(Context activity) {
        return LoadPref(activity,APP_USER);
    }
    public static String getConsultantId(Context activity) {
        return LoadPref(activity,CONSULTANT_ID);
    }


    public static String getStatusType(Context activity) {
        return LoadPref(activity,STORED_ALLSTATUS);
    }

    public static void setActiveMobileNo(Context context, String phoneNo) {
        Utility.SavePref(context,ACTIVE_PHONE_NO,phoneNo);
    }

    public static String getActivePhoneNo(Context context) {
        return LoadPref(context,ACTIVE_PHONE_NO);
    }
}