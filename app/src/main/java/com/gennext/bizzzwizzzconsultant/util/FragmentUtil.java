package com.gennext.bizzzwizzzconsultant.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;


public class FragmentUtil {

	public static boolean hadFragment(AppCompatActivity activity) {
		return activity.getSupportFragmentManager().getBackStackEntryCount() != 0;
	}

    public static Fragment getFragmentByTag(AppCompatActivity appCompatActivity, String tag){
        return appCompatActivity.getSupportFragmentManager().findFragmentByTag(tag);
    }

    protected void replaceFragment(AppCompatActivity activity, Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void setFragment(AppCompatActivity activity, Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void addFragment(AppCompatActivity activity, Fragment fragment, String tag) {
        addFragment(activity,fragment, android.R.id.content, tag);
    }

    public void addFragment(AppCompatActivity activity, Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }
    public void addFragmentWithoutBackStack(AppCompatActivity activity, Fragment fragment, String tag) {
        addFragmentWithoutBackStack(activity,fragment, android.R.id.content, tag);
    }
    public void addFragmentWithoutBackStack(AppCompatActivity activity, Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.commit();
    }

    public void setUnSetFragment(FragmentManager manager, int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1) {
        FragmentTransaction ft = manager.beginTransaction();
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide other fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }

        ft.commit();
    }
//	public static void replaceFragment(AppCompatActivity activity, int contentId, BaseFragment fragment) {
//		FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
//
//		transaction.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_left_out);
//
//		transaction.replace(contentId, fragment, fragment.getClass().getSimpleName());
//
//		transaction.addToBackStack(null);
//		transaction.commit();
//	}


//	public static void addFragment(AppCompatActivity activity, int contentId, BaseFragment fragment) {
//		FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
//
//		transaction.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_left_out);
//
//		transaction.add(contentId, fragment, fragment.getClass().getSimpleName());
//
//		transaction.commit();
//	}
//
//	public static void removeFragment(AppCompatActivity activity, BaseFragment fragment) {
//		activity.getSupportFragmentManager().beginTransaction()
//			.remove(fragment)
//			.commit();
//	}
//
//
//	public static void showFragment(AppCompatActivity activity, BaseFragment fragment) {
//		activity.getSupportFragmentManager().beginTransaction()
//			.show(fragment)
//			.commit();
//	}
//
//	public static void hideFragment(AppCompatActivity activity, BaseFragment fragment) {
//		activity.getSupportFragmentManager().beginTransaction()
//			.hide(fragment)
//			.commit();
//	}
//
//	public static void attachFragment(AppCompatActivity activity, BaseFragment fragment) {
//		activity.getSupportFragmentManager().beginTransaction()
//			.attach(fragment)
//			.commit();
//	}
//
//	public static void detachFragment(AppCompatActivity activity, BaseFragment fragment) {
//		activity.getSupportFragmentManager().beginTransaction()
//			.detach(fragment)
//			.commit();
//	}


}