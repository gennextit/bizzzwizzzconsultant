package com.gennext.bizzzwizzzconsultant;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.gennext.bizzzwizzzconsultant.app.Dashboard;
import com.gennext.bizzzwizzzconsultant.model.CallModel;
import com.gennext.bizzzwizzzconsultant.util.ApiCall;
import com.gennext.bizzzwizzzconsultant.util.AppSettings;
import com.gennext.bizzzwizzzconsultant.util.AppUser;
import com.gennext.bizzzwizzzconsultant.util.CallLogHelper;
import com.gennext.bizzzwizzzconsultant.util.JsonParser;
import com.gennext.bizzzwizzzconsultant.util.L;
import com.gennext.bizzzwizzzconsultant.util.RequestBuilder;

import java.util.ArrayList;

public class MainActivity extends NavDrawer{

    public static final int NOTI_REQ = 100;
    private String callFilter;
    private CallLogHelper callLogHelper;

    @Override
    protected String setNavDrawer(Toolbar toolbar) {
        return getSt(R.string.app_name);
    }

    @Override
    protected Fragment setDefaultScreen(Fragment fragment) {
        return Dashboard.newInstance();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callLogHelper=new CallLogHelper();
        updateCallRecord();
    }

    private void updateCallRecord() {
        callFilter= AppUser.getActivePhoneNo(MainActivity.this);
        if(!callFilter.equals("")) {
            CallModel model=callLogHelper.getCallLogs(getContentResolver(),callFilter);
            if(model.getList()!=null &&model.getList().size()>0){
                new UpdateDateToServer(getApplicationContext()).execute(AppSettings.UDPATE_SERVER_RECORD);
            }
        }
    }

    public class UpdateDateToServer extends AsyncTask<String,Void,Void>{
        private ArrayList<CallModel> list;
        Context context;

        public UpdateDateToServer(Context applicationContext) {
            this.context=applicationContext;
        }

        @Override
        protected Void doInBackground(String... params) {
            String consultantId = AppUser.getConsultantId(context);
            String callJson="",response="";
            CallModel model=callLogHelper.getCallLogs(getContentResolver(),callFilter);
            list=model.getList();
            if(list!=null&&list.size()>0){
                callJson=JsonParser.toCalHistoryJson(list);
                response = ApiCall.POST(params[0], RequestBuilder.UpdateCRecord(consultantId, callJson));
            }
            L.m(response);
            return null;
        }
    }


//    @Override
//    public Loader<Cursor> onCreateLoader(int loaderID, Bundle args) {
//
//        switch (loaderID) {
//            case URL_LOADER:
//                // Returns a new CursorLoader
//                return new CursorLoader(
//                        this,   // Parent activity context
//                        CallLog.Calls.CONTENT_URI,        // Table to query
//                        null,     // Projection to return
//                        null,            // No selection clause
//                        null,            // No selection arguments
//                        null             // Default sort order
//                );
//            default:
//                return null;
//        }
//
//    }

//    @Override
//    public void onLoadFinished(Loader<Cursor> loader, Cursor managedCursor) {
//        Log.d(TAG, "onLoadFinished()");
//
//
//        StringBuilder sb = new StringBuilder();
//
//        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
//        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
//        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
//        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
//
//        CallModel callModel=new CallModel();
//        callModel.setList(new ArrayList<CallModel>());
//        while (managedCursor.moveToNext()) {
//            String phNumber = managedCursor.getString(number);
//            if (phNumber.contains(callFilter)) {
//                String callType = managedCursor.getString(type);
//                String callDate = managedCursor.getString(date);
//                Date callDayTime = new Date(Long.valueOf(callDate));
//                String callDuration = managedCursor.getString(duration);
//                String dir = null;
//
//                int callTypeCode = Integer.parseInt(callType);
//                switch (callTypeCode) {
//                    case CallLog.Calls.OUTGOING_TYPE:
//                        dir = "Outgoing";
//                        break;
//
//                    case CallLog.Calls.INCOMING_TYPE:
//                        dir = "Incoming";
//                        break;
//
//                    case CallLog.Calls.MISSED_TYPE:
//                        dir = "Missed";
//                        break;
//                }
//
//                CallModel slot=new CallModel();
//                slot.set(phNumber,dir,callDayTime.toString(),callDuration);
//                callModel.getList().add(slot);
//
////                sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
////                        + dir + " \nCall Date:--- " + callDayTime
////                        + " \nCall duration in sec :--- " + callDuration);
////                sb.append("\n----------------------------------");
//            }
//        }
//
//
//        managedCursor.close();
//
//        new UpdateDateToServer(getApplicationContext(),callModel.getList()).execute(AppSettings.UDPATE_SERVER_RECORD);
//
////        PopupAlert.newInstance("Alert",sb.toString(),PopupAlert.POPUP_DIALOG)
////                .show(getSupportFragmentManager(),"pop");
//    }

//    @Override
//    public void onLoaderReset(Loader<Cursor> loader) {
//        Log.d(TAG, "onLoaderReset()");
//    }


}